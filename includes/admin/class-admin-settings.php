<?php
/* Code added by Prashant Walke */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class WCLWidget {

    public function __construct() {
        add_action('init', array('WCLWidget', 'init'));
        add_action('wp_enqueue_scripts', array('WCLWidget', 'admin_scripts'));
    }

    static function version() {
        return VERSION;
    }

    static function init() {
        add_action('admin_menu', array('WCLWidget', 'adminPage')); 
    }

    static function admin_scripts() {
        wp_enqueue_script('jquery');
if (!is_front_page()) {
    
        wp_enqueue_style('wpjplist', WPE_PLUGIN_URL . "assets/css/jplist.demo-pages.min.css");
        wp_enqueue_style('wpjplist');

        wp_enqueue_style('wpjplista', WPE_PLUGIN_URL . "assets/css/jplist.core.min.css");
        wp_enqueue_style('wpjplista');
        wp_enqueue_style('wpjplistb', WPE_PLUGIN_URL . "assets/css/jplist.textbox-filter.min.css");
        wp_enqueue_style('wpjplistb');
        wp_enqueue_style('wpjplistc', WPE_PLUGIN_URL . "assets/css/jplist.pagination-bundle.min.css");
        wp_enqueue_style('wpjplistc');

        wp_enqueue_style('wpjplistd', WPE_PLUGIN_URL . "assets/css/jplist.history-bundle.min.css");
        wp_enqueue_style('wpjplistd');
        wp_enqueue_style('wpjpliste', WPE_PLUGIN_URL . "assets/css/jplist.filter-toggle-bundle.min.css");
        wp_enqueue_style('wpjpliste');
        wp_enqueue_style('wpjplistf', WPE_PLUGIN_URL . "assets/css/jplist.views-control.min.css");
        wp_enqueue_style('wpjplistf');

        wp_enqueue_style('wclstyle', WPE_PLUGIN_URL . "assets/css/wcl_style.css");
        wp_enqueue_style('wclstyle');

        wp_enqueue_script('wpjplistjs', WPE_PLUGIN_URL . "assets/js/jplist.core.min.js");
        wp_enqueue_script('wpjplistjs');

        wp_enqueue_script('wpjplistjsa', WPE_PLUGIN_URL . "assets/js/jplist.sort-bundle.min.js");
        wp_enqueue_script('wpjplistjsa');
        wp_enqueue_script('wpjplistjsb', WPE_PLUGIN_URL . "assets/js/jplist.textbox-filter.min.js");
        wp_enqueue_script('wpjplistjsb');
        wp_enqueue_script('wpjplistjsc', WPE_PLUGIN_URL . "assets/js/jplist.pagination-bundle.min.js");
        wp_enqueue_script('wpjplistjsc');
        wp_enqueue_script('wpjplistjscp', WPE_PLUGIN_URL . "assets/js/jplist.history-bundle.min.js");
        wp_enqueue_script('wpjplistjscp');
        wp_enqueue_script('wpjplistjsd', WPE_PLUGIN_URL . "assets/js/jplist.filter-toggle-bundle.min.js");
        wp_enqueue_script('wpjplistjsd');
        wp_enqueue_script('wpjplistjse', WPE_PLUGIN_URL . "assets/js/jplist.views-control.min.js");
        wp_enqueue_script('wpjplistjse');
}
    }

    static function adminPage() {
       // add_menu_page('WCL Widget', 'WCL Widget', 'update_plugins', 'WCLWidget', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        add_menu_page('WCL Widget', 'WCL Widget', 'update_plugins', 'WCLWidget-Setting', array('WCLWidget', 'renderAdminPage'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
       add_submenu_page('WCLWidget-Setting', 'Setting', 'Setting', 'manage_options', 'WCLWidget-Setting', array('WCLWidget', 'renderAdminPage'));
// events
        add_menu_page('WCL Events', 'WCL Events', 'update_plugins', 'events', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        add_submenu_page('events', 'Create Event', 'Create Event', 'manage_options', 'create_event', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('events', 'My Events', 'My Event', 'manage_options', 'my_events', array('WCLWidget', 'WCLWidget_Setting'));
				 add_submenu_page('events', 'Go Live Now', 'Go Live Now', 'manage_options', 'golive-event', array('WCLWidget', 'WCLWidget_Setting'));

// videos
        add_menu_page('WCL Videos', 'WCL Videos', 'update_plugins', 'videos', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        add_submenu_page('videos', 'Upload Video', 'Upload Video', 'manage_options', 'video_upload', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('videos', 'My Videos', 'My Videos', 'manage_options', 'my_videos', array('WCLWidget', 'WCLWidget_Setting'));
// Channels
        add_menu_page('WCL Channels', 'WCL Channels', 'update_plugins', 'channels', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        add_submenu_page('channels', 'Create Channel', 'Create Channel', 'manage_options', 'create_channel', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('channels', 'Venue Mgr', 'Venue Mgr', 'manage_options', 'venue_mgr', array('WCLWidget', 'WCLWidget_Setting'));
// News
        add_menu_page('WCL News', 'WCL News', 'update_plugins', 'news', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        add_submenu_page('news', 'Add News', 'Add News', 'manage_options', 'add_news', array('WCLWidget', 'WCLWidget_Setting'));
// ALERTS
        add_menu_page('WCL Alerts', 'WCL Alerts', 'update_plugins', 'alerts', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico'); 
        add_submenu_page('alerts', 'Add New Alert', 'Add New Alerts', 'manage_options', 'add_alert', array('WCLWidget', 'WCLWidget_Setting'));
		 
// Settings
        add_menu_page('WCL Settings', 'WCL Settings', 'update_plugins', 'manage_event_category', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        // settings sub menus
        add_submenu_page('manage_event_category', 'Manage Event Category', 'Manage Event Category', 'manage_options', 'manage_event_category', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Manage Video Category', 'Manage Video Category', 'manage_options', 'manage_Video_category', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Manage Tags', 'Manage Tags', 'manage_options', 'manage_tags', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Layout', 'Layout', 'manage_options', 'layout', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Spotlight Page', 'Spotlight Page', 'manage_options', 'spotlight_page', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Templates Page', 'Templates Page', 'manage_options', 'templates_page', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Customize Home', 'Customize Home', 'manage_options', 'customize_home', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Banners', 'Banners', 'manage_options', 'banners', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Flash Banners', 'Flash Banners', 'manage_options', 'flash_banners', array('WCLWidget', 'WCLWidget_Setting'));
		 add_submenu_page('manage_event_category', 'Add Members', 'Add members', 'manage_options', 'add_users', array('WCLWidget', 'add_wcl_users'));
       // add_submenu_page('manage_event_category', 'Add Members', 'Add Members', 'manage_options', 'add_members', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Manage Members', 'Manage Members', 'manage_options', 'manage_members', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Manage Member Tags', 'Manage Member Tags', 'manage_options', 'manage_member_tags', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Restricted Words', 'Restricted Words', 'manage_options', 'restricted_words', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Download video settings', 'Download video settings', 'manage_options', 'Download_video_settings', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('manage_event_category', 'Stream settings', 'Stream settings', 'manage_options', 'stream_settings', array('WCLWidget', 'WCLWidget_Setting'));  
		add_submenu_page('manage_event_category', 'Video Layout settings', 'Video Layout settings', 'manage_options', 'video_layout', array('WCLWidget', 'WCLWidget_Setting'));
       
        // Reports
        add_menu_page('WCL Reports', 'WCL Reports', 'update_plugins', 'event_report', array('WCLWidget', 'WCLWidget_Setting'), WPE_PLUGIN_URL . '/assets/images/wcl.ico');
        // settings sub menus
        add_submenu_page('event_report', 'Event Reports', 'Event Reports', 'manage_options', 'event_report', array('WCLWidget', 'WCLWidget_Setting'));
        add_submenu_page('event_report', 'Video Reports', 'Video Reports', 'manage_options', 'video_report', array('WCLWidget', 'WCLWidget_Setting'));



        if (isset($_GET['page']) && ($_GET['page'] == "WCLWidget" || $_GET['page'] == "WCLWidget-Setting")) {
            wp_enqueue_style('wpbootstrapcss', WPE_PLUGIN_URL . "assets/css/bootstrap.min.css");
            wp_enqueue_style('wpbootstrapcss');
        }


    }

    static function add_wcl_users() {
		 
        if(isset($_POST['upload_user']))
        {
			  
            $file = $_FILES['excelFormat']['tmp_name'];
			if($file!=""){
            $handle = fopen($file, "r");
			if( $handle == true ){
            $dataFinal = array();
            $i = 0;  
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    $dataFinal[$i][] =  $data[$c];
                }
                $i++;
            }
            unset($dataFinal[0]);

            fclose($handle);
		
            if(!empty($dataFinal))
            {
                $result = array();
                foreach($dataFinal as $userData)
                {
                    $f_name = $userData[0];
                    $l_name = $userData[1];
                    $username = $userData[2];
                    $email_address = $userData[3];
                    $password = $userData[4];
                    $role = $userData[5]; 
                    $result[] =  add_users_wcl($f_name,$l_name,$username,$password,$email_address,$role,true); 
						 
                }
               ?>
                <div class="notice notice-success is-dismissible">
                    <?php foreach($result as $message){
                        echo ' <p> '.$message.'</p>';
                    }
                    ?>
                  <p><strong>Data has been imported.</strong></p>
                </div>
                <?php
            }
			}
			}
			else{
			?>
			<div class="notice notice-success is-dismissible"> 
                  <p><strong>File not found.</strong></p>
			</div>
			<?php
			}

        }
        ?>

		<style>
		 .add_users_page *{-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
		 .add_users_page .form-white-bg-back h1.form-heading { border: none; line-height: normal; background: #ececec; padding: 10px 20px; font-size: 20px; text-align: center; text-transform: uppercase; font-weight: 600; margin:0 0px 30px 0px; -webkit-border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; border-radius: 4px 4px 0 0; }
		
		.add_users_page .row{display:flex; flex-wrap:wrap; margin-left:-15px; margin-right:-15px;}
		.add_users_page [class*=col-]{padding-right:15px;padding-left:15px; width:100%;}
		.add_users_page .col-sm-3 {width:25%;}
		.add_users_page .col-sm-5 {width:41.66666667%;}
		.add_users_page .col-sm-4 {width: 33.33333333%;}
		.add_users_page .col-sm-6 {width:50%;}
		.add_users_page form input[type="file"] {display: block; width: 100%; height:38px; padding:6px 12px!important; font-size: 14px; line-height: 1.42857143; color: #fff; background-color: transparent; background-image: none; border: 1px solid #ccc; border-radius: 4px;}
		.add_users_page .text-center{text-align:center;}
		.add_users_page .text-right{text-align:right;}
		.add_users_page h3 {font-size: 15px; font-weight: bold; }
		.add_users_page .form-control{display: block; width: 100%; height:38px!important; padding:6px 12px!important; font-size: 14px; line-height: 1.42857143; color: #fff; background-color: #100a19; background-image: none; border: 1px solid #ccc; border-radius: 4px; margin-bottom:25px;min-width: 100%;}
		.add_users_page .padding-remove{padding-left:0; padding-right:0;}
		.add_users_page .btn { display: inline-block; padding:8px 22px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.42857143; text-align: center; white-space: nowrap; vertical-align: middle; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; background:#ff0057; color:#FFF; text-transform: uppercase;}
        .add_users_page .btn:hover,
        .add_users_page .btn:focus {
            background: transparent;
            border-color: #ff0057;
        }
         .message span.success_ {background: green;display: block;padding: 5px 0;margin: 15px 0 0;text-align: center;color: #fff;font-size: 15px;}
         .message span.error_ {background: #e86161;display: block;  margin: 15px 0 0;text-align: center;color: #fff; padding: 5px 0;}
         .add_users_page .form-white-bg-back h1.form-heading {
                background: transparent;
                color: #ff0057;
            }
            .add_users_page select.form-control:focus {
                border-color: #ccc;
                color: #fff;
                box-shadow: inherit;
            }
            .add_users_page * {
                color: #fff;
            }
            .add_users_page .form-control::-webkit-input-placeholder,
            .add_users_page form input[type="file"]::-webkit-input-placeholder {
              color: #fff;
            }
            .add_users_page .form-control::-moz-placeholder,
            .add_users_page form input[type="file"]::-moz-placeholder { 
              color: #fff;
            }
            .add_users_page .form-control:-ms-input-placeholder,
            .add_users_page form input[type="file"]:-ms-input-placeholder { 
              color: #fff;
            }
            .add_users_page .form-control:-moz-placeholder,
            .add_users_page form input[type="file"]:-moz-placeholder {
              color: #fff;
            }


		</style>



       <div id="container" class="bodyScheme bodySchemeSlideUp add_users_page" style="-webkit-overflow-scrolling:touch; overflow:hidden; overflow-y:auto; height: 100%; background: #100a19; max-width: 1170px; margin-top: 30px; padding: 30px;">
            

    <div class="container padding-remove">
     <div class="form-white-bg-back">
        <h1 class="form-heading">Create Member</h1>

          <div class="col-sm-12 col-xs-12 padding-remove">  

          <form name="uploadExcel" action="" method="post" enctype="multipart/form-data" >
				 <div class="row">
                    <div class="col-sm-3 col-xs-12 edit-member">Upload CSV sheet</div>
                    <div class="col-sm-5 col-xs-12 edit-member">
                     <input type="file" name="excelFormat">
                          <small><a href="<?php echo site_url();?>/wp-content/plugins/wcl_widget/includes/admin/add_member.csv" class="">Click here to download demo csv</a></small>
                     </div>
                       <div class="col-sm-4 col-xs-12 edit-member">
                            <input type="submit" value="Upload" name="upload_user" class="btn btn-primary all-btns">
                        </div>
						 </div>
          </form>
				
           <div class="col-sm-12 col-xs-12 text-center"> <img src="https://wclst.worldcastlive.com/tpwidget/images/or.png"> </div>
       

   

            <div class="col-sm-12 col-xs-12 padding-remove"><h3>Enter Member Details :</h3></div>
            
            <form id="userForm" name="userForm" action="" method="post" enctype="application/www-form-urlencoded" onSubmit="return submitFormData()">
                <div id="user0" class="separatorCustom">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <input class="form-control reset-form" type="text" name="f_name" value="" placeholder="First Name" required/>
                        </div>
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <input class="form-control reset-form" type="text" name="l_name" value="" placeholder="Last Name" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <input class="form-control reset-form" type="text" name="email" value="" placeholder="Email ID" required/>
                            <input class="form-control" type="hidden" name="action" value="add_user_ajax" />
                        </div>
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <select class="form-control" name="role" required/>
                             <option value="1">Viewer</option>
                             <option value="2">Artist</option>
                            </select>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <input class="form-control reset-form" type="text" name="u_name" value="" placeholder="Username" required/>
                        </div>
                        <div class="col-sm-6 col-xs-12 edit-member">
                            <input class="form-control reset-form" type="text" name="password" value="" placeholder="Password" required/>
                        </div>

                    </div>
                 </div>
                <div class="col-sm-12 col-xs-12 text-right padding-remove">
                    <input id="submit_btn_" class="btn btn-primary all-btns" type="submit" value="Create" >
                </div>
            </form>
            <div class="message" style="display:none;"></div>
             
             </div><!-- row end -->
          <div class="clearfix"></div>
        </div>

    <div class="clearfix"></div>
   </div> 
</div>
<script>
  function submitFormData() {
        var get_data = jQuery('#userForm').serialize();
             jQuery.post(ajaxurl, get_data, function (response) {
                 var data = JSON.parse(response);
                 jQuery('.message').html(data.message);
                 jQuery('.message').show();
                 if(data.status == 1) {
                     jQuery('#userForm .reset-form').val('');
                 }
                 setTimeout(function(){
                     jQuery('.message').hide();
                 }, 5000);

             });
         return false;
     }
</script>
        <?php

    }
    static function renderAdminPage() {
        if (isset($_POST['submit']) && isset($_POST['key'])) {
            update_option('wp_event_key', sanitize_text_field($_POST['key']));
        }
        if (isset($_POST['submit']) && isset($_POST['End_Point'])) {
            update_option('End_Point', sanitize_text_field($_POST['End_Point']));
        }
        
        if (isset($_POST['submit']) && isset($_POST['environment'])) {
            update_option('wp_event_environment', sanitize_text_field($_POST['environment']));
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_list_height'])) {
            update_option('wp_event_list_height', sanitize_text_field($_POST['wp_event_list_height']));
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_list_width'])) {
            update_option('wp_event_list_width', sanitize_text_field($_POST['wp_event_list_width']));
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_list_grid_count'])) {
            update_option('wp_event_list_grid_count', sanitize_text_field($_POST['wp_event_list_grid_count']));
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_custom_css'])) {
            update_option('wp_event_custom_css', sanitize_text_field($_POST['wp_event_custom_css']));
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_switch_view'])) {
            update_option('wp_event_switch_view', sanitize_text_field($_POST['wp_event_switch_view']));
        } else {
            update_option('wp_event_switch_view', 0);
        }

        if (isset($_POST['submit']) && isset($_POST['wp_event_filter'])) {
            update_option('wp_event_filter', sanitize_text_field($_POST['wp_event_filter']));
        } else {
            update_option('wp_event_filter', 0);
        }
        //   if (isset($_POST['submit']) && isset($_POST['endpoint_url'])) {
        //       update_option('wp_event_endpoint_url', sanitize_text_field($_POST['endpoint_url']));
        //    }//Commented in V.1.1 01-09-2015
        $wp_event_key = get_option('wp_event_key');
        $wp_End_Point=get_option('End_Point');
        $wp_event_environment = get_option('wp_event_environment');
        $wp_event_list_height = get_option('wp_event_list_height');
        $wp_event_list_width = get_option('wp_event_list_width');
        $wp_event_list_grid_count = get_option('wp_event_list_grid_count');
        $wp_event_switch_view = get_option('wp_event_switch_view');
        $wp_event_custom_css = get_option('wp_event_custom_css');
        $wp_event_filter = get_option('wp_event_filter');
        //   $wp_event_endpoint_url = get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
        ?>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php _e('WCL Setting', 'wcl-widget'); ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">  
                    <form action="" method="post">
                        <div class="col-md-2"><?php _e('Domain specific access key:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><input type="text" class="form-control" placeholder="Enter domain specific access key" name="key" value="<?php echo $wp_event_key; ?>">
                            <br><?php _e('Domain specific access key. This will be provided by the WCL admin when the domain is registered', 'wcl-widget'); ?>
                        </div>
                        
                        <div class="col-md-2"><?php _e('Domain specific End Point :', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><input type="text" class="form-control" placeholder="Enter domain specific End Point" name="End_Point" value="<?php echo $wp_End_Point; ?>">
                            <br><?php _e('Domain specific End Point. This will be provided by the WCL admin when the domain is registered', 'wcl-widget'); ?>
                        </div>

                        
                        <div class="col-md-2"><hr><br><?php _e('Environment:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr>
                            <input type="radio" <?php echo $wp_event_environment == "live" ? 'checked' : ''; ?> name="environment" value="live"><?php _e('Live', 'wcl-widget'); ?><br/>
                            <input type="radio" <?php echo $wp_event_environment == "sandbox" ? 'checked' : ''; ?> name="environment" value="sandbox"><?php _e('Sandbox', 'wcl-widget'); ?>
                            <br><?php _e('If your domain registered on live environment (http://www.worldcastlive.com) then select "Live" Environment.', 'wcl-widget'); ?>                            
                            <br><?php _e('If your domain registered on sandbox environment (http://' . WPE_ENV . '.worldcastlive.com) then select "Sandbox" Environment (For testing purposes only).', 'wcl-widget'); ?>                                
                        </div>
                        <div class="col-md-12"><hr><h4><?php _e('Layout Setting:', 'wcl-widget'); ?></h4></div>
                        <div class="col-md-2"><hr><?php _e('Height:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr><input type="text" class="form-control" placeholder="Enter height for liting" name="wp_event_list_height" value="<?php echo $wp_event_list_height; ?>">
                            <br><?php _e('Enter the height for event/video item', 'wcl-widget'); ?>
                        </div>

                        <div class="col-md-2"><hr><?php _e('Width:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr><input type="text" class="form-control" placeholder="Enter width for liting" name="wp_event_list_width" value="<?php echo $wp_event_list_width; ?>">
                            <br><?php _e('Enter the width for event/video item', 'wcl-widget'); ?>
                        </div>

                        <div class="col-md-2"><hr><?php _e('No of item in grid:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr><input type="text" min="1" max="5" class="form-control"  name="wp_event_list_grid_count" value="<?php echo $wp_event_list_grid_count; ?>">
                            <br><?php _e('Enter the no of item show in grid [enter range : 1-5]', 'wcl-widget'); ?>
                        </div>

                        <div class="col-md-2"><hr><?php _e('Enable Switch View:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr>  <input type="checkbox" <?php echo $wp_event_switch_view == "1" ? 'checked' : ''; ?> name="wp_event_switch_view" value="1">
                        </div>

                        <div class="col-md-2"><hr><?php _e('Enable Search Filter:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr>  <input type="checkbox" <?php echo $wp_event_filter == "1" ? 'checked' : ''; ?> name="wp_event_filter" value="1">
                        </div>

                        <div class="col-md-2"><hr><?php _e('Custom CSS:', 'wcl-widget'); ?></div>
                        <div class="col-md-10"><hr>  <textarea  name="wp_event_custom_css"><?php echo $wp_event_custom_css ?></textarea>
                            <br><?php _e('Enter custom css like <br>.title{color:red}', 'wcl-widget'); ?>

                        </div>

                        <div class="row">  
                            <div class="col-md-2"></div>                        
                            <div class="col-md-10"><hr><input type="submit" class="btn btn-success" name="submit" value="<?php _e('Save', 'wcl-widget'); ?>"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php _e('Documentation', 'wcl-widget'); ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><?php _e('1. Enter Domain specific access key. This will be provided by the WCL admin when the domain is registered ', 'wcl-widget'); ?></li>   
                            <hr><li><?php _e('2. Use this shortcode [wcl_widget] in any page', 'wcl-widget'); ?></li>
                            <li><?php _e('3. Set width and height for iframe: use this shortcode [wcl_widget width=935 height=730]', 'wcl-widget'); ?></li>
                            <li><?php _e('4. Set only width for iframe: use this shortcode [wcl_widget width=935]', 'wcl-widget'); ?></li>
                            <li><?php _e('5. Set only height for iframe: use this shortcode [wcl_widget height=730]', 'wcl-widget'); ?></li>
                            <hr><li><?php _e('6. If you have any issue then make sure that following setting are correct:', 'wcl-widget'); ?></li>
                            <ul>
                                <li><?php _e('a. Enter correct Domain specific access key', 'wcl-widget'); ?></li>
                               <!-- <li><?php //_e('b. Enter correct Endpoint','wcl-widget'); ?></li> -->
                                <li><?php _e('c. Select proper Environment', 'wcl-widget'); ?></li>
                                <li><?php _e('d. Make sure that CURL working on your server', 'wcl-widget'); ?></li>
                            </ul>
                            <hr>
                            <li><h4><?php _e('Server Setting :', 'wcl-widget'); ?></h4></li>
                            <li><?php echo 'Curl: ', function_exists('curl_version') ? 'Enabled' : 'Disabled'; ?></li>                                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }


    static function WCLWidget_Setting() {
        // send wpaction to WCL
        $wpaction = "";
        $hide = 0;
         if($_GET['page']!= "WCLWidget"){
             $wpaction = '&wpaction='. $_GET['page'];
             $hide = 1;
         }
        if (isset($atts['width'])) {
            $width = $atts['width'];
        }
        if (isset($atts['height'])) {
            $height = $atts['height'];
        }
        if (isset($width) && !empty($width)) {
            $width = 'width=' . $width . 'px';
        } else {
            $width = 'width=1170px';
        }
        if (isset($height) && !empty($height)) {
            $height = 'height=' . $height . 'px';
        } else {
            $height = 'height=1130px';
        }

        $return = "";


        $wp_End_Point=get_option('End_Point');
        if(empty($wp_End_Point)){
            $wp_End_Point='frptv';
        }

        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        $endpoint_env = get_option('wp_event_environment');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = 'http://worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }
        // echo $token;exit;
        $return = "";
        $return .='<div id="videcontainerwrapper" class="videcontainerwrapper"  style="display:none;" ><div id="videocontainer" style="display:none;">here is player</div></div>';
        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
              /**
               * We are pointing everything to new2.worldcastlive.com for now
               */

            if (!empty($wp_event_endpoint_url)) {
                $return .= "<iframe id='wcl_widget_iframe' allow='microphone; camera' style='margin-left:0%; width:100%; max-width:1170px;' src='https://" . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?hide=".$hide."&verify=" . $wp_End_Point.$wpaction."' frameborder='0' $width $height scrolling='yes' ></iframe>";
            } else {
                $return .= "<iframe id='wcl_widget_iframe' allow='microphone; camera' style='margin-left:0%; width:100%; max-width:1170px;' src='https://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=".$hide."&verify=" . $wp_End_Point.$wpaction."' frameborder='0' $width $height scrolling='yes' ></iframe>";
            }
        } else {
            $return .= "<iframe id='wcl_widget_iframe' allow='microphone; camera' style='margin-left:0%; width:100%; max-width:1170px;' src='https://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=".$hide."&verify=". $wp_End_Point .$wpaction."' frameborder='0' $width $height scrolling='yes' ></iframe>";
        }

        $return .= '<script>
            window.onload = function () {
                if (typeof history.pushState === "function") {
                      history.pushState("jibberish", null, null);
                    window.onpopstate = function () {
                         history.pushState(\'newjibberish\', null, null);
                       window.location=document.referrer;
                    };
                }
            }
          </script>';
        ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php _e('WCL Widget', 'wcl-widget'); ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">  
        <?php
        // if(!empty($token)){
        echo $return;
        //   }else{
        //        echo "Invalid Access : Login as WCL Widget admin";
        //    }
        ?>
                </div>  
            </div>
        </div>
                    <?php
                }

            }

            // end WCLWidget
            $WCLWidget = new WCLWidget();
add_action( 'wp_ajax_add_user_ajax', 'add_user_ajax_func' );
add_action( 'wp_ajax_nopriv_add_user_ajax', 'add_user_ajax_func' );

function add_user_ajax_func() {
    global $wpdb; // this is how you get access to the database
    $username = $_POST['u_name'];
    $f_name = $_POST['f_name'];
    $role = $_POST['role'];
    $l_name = $_POST['l_name'];
    $email_address = $_POST['email'];
    $password = $_POST['password'];
    if (  email_exists( $email_address ) ) {
        echo json_encode(array( 'status'=>0, "message"=>"<span class=error_>Error: Email Already exist.</span>" ));
        wp_die();
    }
    if (  username_exists( $username ) ) {
        echo json_encode(array('status' => 0, "message" => "<span class=error_>Error: Username Already exist.</span>"));
        wp_die();
    }

    echo add_users_wcl($f_name,$l_name,$username,$password,$email_address,$role);

    wp_die();
}
function add_users_wcl($f_name,$l_name,$username,$password,$email_address,$role,$bulk = false){
	
	 	if ($role == 2 || $role == 'artist' || $role == 'Artist') {
			$user_role = 2;
		}
		if ($role == 1 || $role == 'viewer' || $role == 'Viewer') {
			$user_role = 1;
		}
    $userdata = array(
        'first_name'  => $f_name,
        'last_name'  => $l_name,
        'user_login'  => $username,
        'user_pass'   => $password,
        'user_url'    => $user_role,
        'user_email'   => $email_address,
    ); 
 
	$user_id = wp_insert_user( $userdata );  
	$error = "";
	if( is_wp_error( $user_id  ) )
	{
       $error =  $user_id->get_error_message();    
	}
	 
	 
    if(!isset($user_id->errors)) { 
        $roleName = 'Student';
        if ($role == 2 || $role == 'artist' || $role == 'Artist') {
            $roleName = 'Teacher';
        }
        cloneRole($roleName);
        $user = new WP_User($user_id);
        $user->add_role($roleName);
        $wpevent = new WPEVENT();
		
        $wpevent->wcl_register_user($email_address, $f_name, $l_name, $role);
        if($bulk){
            return ' User ' .$username .' created successfully';
        }
        else{
            echo json_encode(array( 'status'=>1, "message"=>"<span class=success_>User created successfully.</span>" ));
        }

    }
    else{
		if(isset($user_id->errors['existing_user_login'][0])){
			 $error =  $user_id->errors['existing_user_login'][0];
		}
		if(isset($user_id->errors['existing_user_email'][0])){
			 $error =  $user_id->errors['existing_user_email'][0];
		}
		
        if($bulk){
			if($f_name != ""){
				return ' Error : ' .$username .' Unable to create, '. $error;
			}
        }
        else{
            echo json_encode(array( 'status'=>0, "message"=>"<span class=error_>Error: please try again.".$error."</span>" ));
        }
    }

}
function cloneRole($role)
{
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('subscriber');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role($role, $role, $adm->capabilities);
}
?>
