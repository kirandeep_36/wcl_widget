<?php


// Creating the widget 
class wpwclcategory extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpwclcategory', 

// Widget name will appear in UI
__('WCL Videos', 'wcl-widget'), 

// Widget description
array( 'description' => __( 'WCL Videos based on categories', 'wcl-widget' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
    error_log("cat widget start time");
    error_log(time());
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
//if ( ! empty( $title ) )
//$title= $args['before_title'] . $title . $args['after_title'];
$wcl_filter='';

$viewMoreLink='';
 $viewMoreLink="?category=All";
if (isset($instance['category']) && $instance['category']!="") {

        $wcl_filter.="/category/" . $instance['category'];
        if(!empty($instance['category']))
         $viewMoreLink="?category=" .$instance['category'];
else
     $viewMoreLink="?category=All";
    }else{
        $wcl_filter.='';
    }
     $viewMoreLink = "?top_videos=30".'&categoryName=TOP-VIDEOS';
      $wcl_filter.="/days/30";
    $viewMore=get_page_link(get_option("wcl_search_page")).$viewMoreLink;
$wp_event_environment = get_option('wp_event_environment');
 
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
     $link = "?";
     $wcl_filter.="/limit/5";
if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
    }
    
      //obtaint the token first  
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey
        )
    ));
    $return = "";
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);
    $eventList = $response['data']['videoList'];
   // $eventList = array_reverse($eventList);
    $videoPagelink = get_page_by_path( 'top-videos' );
    if(isset($videoPagelink->ID) && !is_null($videoPagelink->ID)){
        $viewMore = get_page_link($videoPagelink->ID);
        $viewMore .= '?categoryName=Top%20Videos';
    }
	$i=1; 
    $return='';
       $return.= '<div class="header-div-bottom header-div-bottom-h2">'.$args['before_title'] . $title . $args['after_title']."</div>";
         $return.= '<div class="cactus-widget-posts ">';
		 foreach ($eventList as $responsearr) {                  

             $return.='<div class="cactus-widget-posts-item">
                <div class="widget-picture">
                    <div class="widget-picture-content">
                        <a href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '" title="'.$responsearr['title'].'">
                            <div class="adaptive">
				
			<img width="94" height="53" src="' . $responsearr['avatar'] . '" alt="'.$responsearr['title'].'">
                            </div>
			<div class="thumb-overlay"></div>
		                    			</a>
		</div> 
			</div>	
                        <div class="cactus-widget-posts-content">
				<h3 class="h6 widget-posts-title">
                                        <a title="'.$responsearr['title'].'" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">'.$responsearr['title'].'</a>
                                </h3>
    			            
                        </div>
            </div>';
             if($i>=3){
                $return.='<p style="margin-left:10px"><a style="color:blue" href='.$viewMore.' title="View More"> View More</a></p>';
                break;
            }
            $i++;
                }
       $return.='</div>';

                 
// This is where you run the code and display the output
$html_content=$return;
   
echo $html_content;
echo $args['after_widget'];
error_log("cat widget end time");
    error_log(time());
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Videos', 'wcl-widget' );
}

if ( isset( $instance[ 'category' ] ) ) {
$category = $instance[ 'category' ];
}
else {
$category = __( '', 'wcl-widget' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category ID:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>" />
<br>Leave blank for all videos.
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';

return $instance;
}
} // Class wpbdpwidget ends here



function wp_wcl_category() {	
        register_widget( 'wpwclcategory' );
}

add_action( 'widgets_init', 'wp_wcl_category' );

