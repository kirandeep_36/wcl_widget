<?php

$wp_event_environment = get_option('wp_event_environment');
$headingTitle = '';
$viewMoreLink = '';
if (isset($atts['title'])) {
    $headingTitle = $atts['title'];
}
if (isset($atts['desc'])) {
    $heading.="<h3>" . $atts['desc'] . "</h3>";
}

if (isset($atts['time'])) {
    $wcl_filter.="/time/" . $atts['time'];
     $viewMoreLink = "?time=" . $atts['time'];
}else{
     $viewMoreLink = "?time=All";
}
$wcl_filter.="/limit/9";
if (isset($atts['category'])) {
    $wcl_filter.="/category/" . $atts['category'];
     $viewMoreLink = "?view=event&title=".$headingTitle."&category=" . $atts['category'];
}else{
     $viewMoreLink = "?view=event&title=".$headingTitle."&category=All";
}

$viewMore = get_page_link(get_option("wcl_search_page")) . $viewMoreLink;

if ($wp_event_environment == "live") {
    $_url = 'http://worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
} else {
    $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
}

if (isset($_GET['page_id'])) {
    $link = "&";
} else {
    $link = "?";
}
$link = "?";

$return = "";
$response = call_wcl_api($_url);
$eventList = $response['data']['eventList'];

//echo "<pre>";print_r($eventList);echo "</pre>";exit;
// $eventList = array_reverse($eventList);

if (isset($atts['type']) && $atts['type'] == '2') {
    $return.='<div class="cactus-categories-slider" style="padding:0px;">
          <div data-auto-play="" class="cactus-slider cactus-slider-wrap slidesPerView ">
        <div class="cactus-slider-content">
            <div class="fix-slider-wrap"> <a class="cactus-slider-btn-prev" href="javascript:;" style="display: block;"><i class="fa fa-angle-left"></i></a> <a class="cactus-slider-btn-next" href="javascript:;" style="display: block;"><i class="fa fa-angle-right"></i></a>
              <div data-per-view="2" data-settings="[&quot;mode&quot;:&quot;cactus-fix-composer&quot;]" class="cactus-swiper-container" style="">
                <div class="swiper-wrapper">
    		';
    $i = 1;
    $j = 1;
    $isLast = 1;
    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 15) {
            $title = substr($responsearr['title'], 0, 12) . "....";
        } else {
            $title = $responsearr['title'];
        }

        if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        if (($i) == 1) {
            $swiperactive = 'swiper-slide-active';
        } else {
            $swiperactive = '';
        }
        $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'];


        if (($i % 3) == 0) {
            $j = 1;

            $rowWidth = '50percent';

            $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

            $return.='<div class="width-' . $rowWidth . '">';

            $return.='<div class="slide-post-item"><a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">
</a>
<div class="adaptive">
<div class="thumb-overlay"></div>
 <a href="' . $videoLink . '" title="' . $button_text . '">
<img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat" '.$button_style.'><a title=" ' . $responsearr['category_name'] . '" href="#">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title" style="margin-bottom: 13px;"><a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">' .$live_text. $responsearr['title'] . '</a></h3>
</div>
';


            $return.='</div></div>';

            $isLast = 1;
        } else {
            $rowWidth = '25percent';
            if ($j == 1) {
                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';
            }
            $return.='<div class="slide-post-item"><a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">
</a>
<div class="adaptive">
<div class="thumb-overlay"></div>
 <a href="' . $videoLink . '" title="' .$button_text . '">
<img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat" '.$button_style.'><a title=" ' . $responsearr['category_name'] . '" href="#">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"  style="margin-bottom: 7px;"><a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">'  .$live_text. $responsearr['title'] . '</a></h3>
  
</div>
';
            if ($j != 1) {

                $return.='</div></div>';
                $isLast = 1;
            } else {
                $isLast = 0;
            }
            $j++;
        }


        //   if(!(($i%3)==0 || ($i%2)==0)){



        $i++;
    }
    if ($isLast == 0) {
        $return.='</div></div>';
    }
    $return.='</div>
</div>
</div>
</div>
</div>
</div>';
} else if (isset($atts['type']) && $atts['type'] == '3') {
    $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

    if (!empty($headingTitle))
       $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

    $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
    $i = 1;
    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 25) {
            $title = substr($responsearr['title'], 0, 22) . "....";
        } else {
            $title = $responsearr['title'];
        }
        if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'];
        $categoryLink = '#';
        $return.='  <div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $button_text . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '"  width="380" height="285"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $button_text . '"> </a>
                                                                    </p><div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    <div class="content-abs-post">
                                                                        <div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                       
<h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' .$button_text . '">'  .$live_text. $title . '</a></h3>
      <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
        if ($i >= 12) {
            $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
            break;
        }
        $i++;
    }

    $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
} else if (isset($atts['type']) && $atts['type'] == '4') {
    $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
         
    if (!empty($headingTitle))
       $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

    $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
    $i = 1;
    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 15) {
            $title = substr($responsearr['title'], 0, 12) . "....";
        } else {
            $title = $responsearr['title'];
        }
         if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'];
        $categoryLink = '#';
        if ($i == 1) {
            $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $button_text . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' .$button_text . '"> </a>
                                                                    </p><div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg" style="margin-top: -39px;">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="'  .$live_text. $videoLink . '" title="">'  .$live_text. $responsearr['title'] . '</a></h3>
                                                                 <!--  <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>                    -->        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
        } else {
            $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $button_text . '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>
                                                                           <div class="thumb-overlay" style="height: 84%;"></div>
                                                                        </a>
                                                                        <!--<p><a href="' . $videoLink . '" title="' . $button_text. '">  </a>
                                                                        </p><div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div> -->
                                                                    </div>
                                                                </div>
                                                                <div class="content" >
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">'  .$live_text. $responsearr['title'] . '</a></h3>
                                                                      <div class="posted-on post-event-page">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
        }
        if ($i >= 4) {
            $return.='<p style="margin-left:10px;    margin-top: -18px;"><a href=' . $viewMore . ' title="View More" style="color:black; "> View More</a></p>';
            break;
        }
        $i++;
    }
    $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
} else if (isset($atts['type']) && $atts['type'] == '5') {
    $return.='<div class="wpb_column vc_column_container vc_col-sm-6">
    <div class="wpb_wrapper">
        <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="4" data-item-in-page="4" data-style="1" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#27aac5" data-title-color="#ffffff" data-shortcode="" data-ids="">
           ';
    if (!empty($headingTitle))
       $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

    $return.='          <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="append-slide-auto" data-page="1">
                            <div class="cactus-listing-wrap">
                                <div class="cactus-listing-config style-1">
                                    <div class="cactus-listing-content">
                                        <div class="cactus-sub-wrap">';
    $i = 1;
    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
       
         if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 15) {
            $title = substr($responsearr['title'], 0, 12) . "....";
        } else {
            $title = $responsearr['title'];
        }
        $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'];
        $categoryLink = '#';
        if ($i == 1) {
            $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $button_text. '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                      <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $button_text. '"> </a>
                                                                    </p><div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">'  .$live_text. $responsearr['title'] . '</a></h3>
                                                                       <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>                      
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                ';
        } else {
            $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $button_text. '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="53"></div>
                                                                           <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $button_text . '"> </a>
                                                                        </p><div class="cactus-note-cat" '.$button_style.'><a href="' . $categoryLink . '" title=" ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">'  .$live_text. $responsearr['title'] . '</a></h3>
                                                                    
                                                                     <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
        }
        if ($i >= 6) {
            $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
            break;
        }
        $i++;
    }
    $return.='</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
} else if (isset($atts['type']) && $atts['type'] == '6') {
    $return.='<div data-settings="" class="cactus-swiper-container">
                <div class="swiper-wrapper">
                
                    <div class="swiper-slide"> <!--Slide item-->
                        
                        <div data-page="1" class="append-slide-auto">
				 			       
                            <!--Listing-->
                            <div class="cactus-listing-wrap">
                                <!--Config-->        
                                <div class="cactus-listing-config style-1"> <!--addClass: style-1 + (style-a -> style-f)-->
                                        
                                    <div class="cactus-listing-content">
                                    
                                        <div class="cactus-sub-wrap">
                                        <div class="cactus-post-item hentry ">';
    $i = 1;
    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 15) {
            $title = substr($responsearr['title'], 0, 12) . "....";
        } else {
            $title = $responsearr['title'];
        }
        if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        if ($i == 1) {
            $return.='<div class="entry-content">
                                        <div class="primary-post-content"> <!--addClass: related-post, no-picture -->
                                            <!--picture-->
                                            <div class="picture">
                                                <div class="picture-content">
                                                    <a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">
                                                        <div class="adaptive">

                                                            <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>                                                                        <div class="thumb-overlay"></div>
<div class="thumb-overlay"></div>
                                                   

                                                    </a>


                                                    <div class="cactus-note-cat" '.$button_style.'><a title=" ' . $responsearr['category_name'] . '" href="#">' . $responsearr['category_name'] . '</a>
                                                    </div>                                                                                                                                                                                                                   
                                                </div>                                                                
                                            </div>

                                            <div class="content">
                                                <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">'  .$live_text. $responsearr['title'] . '</a></h3>

  <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                                            </div>

                                        </div><!--content-->

                                    </div>  
                                    
<div class="fix-right-style-4">';
        } else {
            $return.='<div class="cactus-post-item hentry ">
                                            <!--content-->
                                            <div class="entry-content">
                                                <div class="primary-post-content"> <!--addClass: related-post, no-picture -->

                                                    <!--picture-->
                                                    <div class="picture">
                                                        <div class="picture-content">
                                                            <a title="' . $button_text. '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">
                                                                <div class="adaptive">

                                                                    <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>                                                                        <div class="thumb-overlay"></div>
<div class="thumb-overlay"></div>
                                                              

                                                            </a>


                                                            <div class="cactus-note-cat" '.$button_style.'><a title=" ' . $responsearr['category_name'] . '" href="#">' . $responsearr['category_name'] . '</a>
                                                            </div>                                                                                                                                                                                                                   
                                                        </div>                                                                
                                                    </div>

                                                    <div class="content">
                                                        <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">'  .$live_text. $responsearr['title'] . '</a></h3>
                                                         <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                                                    </div>

                                                </div>

                                            </div><!--content-->

                                        </div>';
        }
        if ($i >= 6) {
            $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
            break;
        }
        $i++;
    }


    $return.='</div>
            
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
} else {
    $return.='<div data-visible="3" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">';
      if (!empty($headingTitle))
       $return.='<div class="cactus-scb header-div-bottom" style="margin-left: 4px;"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';
      
           $return.='     <div class="swiper-wrapper">
             ';

    foreach ($eventList as $responsearr) {
        $date = date_create($responsearr['timestart']);
        if (strlen($responsearr['title']) > 15) {
            $title = substr($responsearr['title'], 0, 12) . "....";
        } else {
            $title = $responsearr['title'];
        }
         if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
            $button_text = "Stream Now";
             $live_text = "<span style='color:red'>Live Event-</span>";
            $button_style='style="background-color: #dc3e3f;"';
        } else {
            $button_text = "View Info";
            $button_style='';
            $live_text='';
        }
        $event_info = 'Date: ' . date_format($date, "M d Y") . '  <br>
                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                   Type: ' . $responsearr['category_name'] . '<br>
                    ' . $responsearr['venue'] . '<br>' . $button_text;
        $return.='<div class="swiper-slide" style="width: 383px; height: 300px;">
                            <div class="carousel-item">
                                <a title="' . $button_text . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">
                                    <img class="lazyload attachment-thumb_375x300" data-original="' . $responsearr['avatar'] . '" width="375" height="211"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"></div>
                                </a>
                                <div class="primary-content">
                                    
				<div class="cactus-note-cat" '.$button_style.'><a title="' . $responsearr['category_name'] . '" href="#">' . $responsearr['category_name'] . '</a>
                </div>                                                                        <h3 class="h6"><a title="' . $button_text. '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '">'  .$live_text. $responsearr['title'] . '</a></h3>
                      <div class="posted-on">
                                ' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . '<br>' . $responsearr['venue'] . '
                                </div>
                    
                                </div>
                            </div>
                        </div>';
    }
    $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div>';
}
   

