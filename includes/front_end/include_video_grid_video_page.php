<?php
 
        $eventList = array_reverse($eventList);
        
        foreach ($eventList as $responsearr) {
           
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 18) {
                $title = substr($responsearr['title'], 0, 15) . "...";
            } else {
                $title = $responsearr['title'];
            }
             $video_type="";
            
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            
            $return.='<!-- item 1 -->
						<div class="list-item item box col-md-4 video-list-item text-center">	

                            <div class="video-holder" data-title="'. $responsearr['title'] .'">
                            <a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> 
                                <img alt="" class="img-responsive" src="' . $responsearr['avatar'] . '" style="width:100%; height: 200px">
                            </a>
                            <div id="play"></div>
                                <div class="footnote title"><a href="'. $videoLink .'">'. $responsearr['title'] .'</a></div>
                            </div>
                        </div>';
        }