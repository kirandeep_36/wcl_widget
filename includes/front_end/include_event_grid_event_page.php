<?php

// $return.= '<script src="https://cdn.rawgit.com/nnattawat/flip/v1.0.19/dist/jquery.flip.min.js">';
$eventList = array_reverse($eventList);
//echo "<pre>";
//print_r($eventList);
//echo "</pre>";exit;
foreach ($eventList as $responsearr) {
    $date = date_create($responsearr['timestart']);
    if (strlen($responsearr['title']) > 30) {
        $title = substr($responsearr['title'], 0, 30) . "....";
    } else {
        $title = $responsearr['title'];
    }
    $button_color = '';
    $button_color_view = '';
    if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
        $button_text = "Stream Now";
        $header_title_text = "Live";
        $button_color = "background:red";
        $button_color_view = "background:red";
    } else if (isset($responsearr['is_past']) && $responsearr['is_past'] == '1') {
        $button_text = "View Info";
        $header_title_text = "Past";
        $button_color = "background:gray";
        $button_color_view = "background:blue";
    } else if (isset($responsearr['is_future']) && $responsearr['is_future'] == '1') {
        $button_text = "View Info";
        $header_title_text = "Upcoming";
        $button_color = "background:blue";
        $button_color_view = "background:blue";
    }

    if (isset($responsearr['free']) && $responsearr['free'] == 1) {
        $paid_status = "active-channel-free-class";
        $paid_status_class = "Free";
    } else {
        $paid_status = "active-channel-paid-class";
        $paid_status_class = "Paid";
    }


    $return.='<div class="eventlist list-item item box col-lg-5ths col-md-3 col-sm-4 card" style="height: 250px;/*margin: 0px 0px 10px 0px !important;*/">';
    if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
        $return.='<div class="live-tag"> </div>';
    }
    $return.='<div class="img front" style="width:94% !important;">
        <div class="' . $header_title_text . ' cactus-note-cat" style="padding-right: 26px;margin-right: 5px;text-shadow: none;' . $button_color . '"><a href="javascipt:void(0);">' . $header_title_text . '</a></div>
           <div class="' . $paid_status_class . ' ' . $paid_status . '"></div> 
								<img  class="lazy imgeventclass" data-original="' . $responsearr['avatar'] . '" style="height: ' . $wp_event_page_list_height . 'px !important;"  height="100" title="' . $responsearr['title'] . '"/>
                                                        <div class="eventlist-content-abs-post-overide-on-video bottom-test-style content-abs-post content-abs-post-overide-on-video" style="margin-top: -84px !important;margin-left: 0px;text-align: left;width: 100%;">
                                                        <h3>' . $title . '<p style="font-size: 17px;">' . date_format($date, "M d Y") . ' ' . date_format($date, "h:i:s A") . ' </p></h3>
                                                        </div>                            
							</div>
							
							<!-- data -->
							<div class="mouse-over1 block1 back">
                                                        <div class="mouse-over-css" style="padding:15px;text-align: center;height: ' . $wp_event_page_list_height . 'px !important;margin: 0px 5px 0px 5px;background-color: rgba(68,68,68,1.0);text-shadow: none;">
                                                        <div class="test-title" style="padding-bottom: 15px;">
								<p class="title eventlistitem" style="color:#FFF!important; "><b>' . ucfirst($responsearr["title"]) . '</b><br>
                                                                    Date: ' . date_format($date, "M d Y") . '  <br>
                                                                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                                                                  Type: ' . $responsearr['category_name'] . '<br>
                                                                    Venue: ' . $responsearr['venue'] . '
                                                               </p>
                                                        </div>
                                                                    <div class="text-center">
                                                                      <a href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '"> 
                                                                        
                                                                          <button type="button" style="' . $button_color_view . '" class="btn btn-danger">' . $button_text . '</button>
                                                                      </a>
                                                       
                                                                  </div>
                                                           </div>       
							</div>
                                                        
						</div>';
}


$return.=' <script type="text/javascript">
      var JF= jQuery.noConflict(); 
    JF(function(){
      // JF(".card").flip();
      
      JF(".card").flip({
        axis: "x", // y or x
        reverse: false, // true and false
        trigger: "hover", // click or hover
        speed: 300
      });
    });
    </script>';


