<?php

/* Code added by Prashant Walke */
add_shortcode('wcl_widget', 'wcl_widget');
add_shortcode('wcl_widget_upload_video', 'wcl_widget_upload_video');
add_shortcode('wcl_get_events_detail', 'wcl_get_events_detail');
add_shortcode('wcl_search', 'wcl_search');
add_shortcode('wcl_search_video_event', 'wcl_search_video_event');
add_shortcode('wcl_search_box', 'wcl_search_box');
add_shortcode('wcl_search_box_in_menu', 'wcl_search_box_in_menu');
add_shortcode('wcl_get_events_list', 'wcl_get_events_list');
add_shortcode('wcl_get_video_list', 'wcl_get_video_list');
add_shortcode('wcl_get_video_slider', 'wcl_get_video_slider');
add_shortcode('wcl_get_events', 'wcl_get_events');
add_shortcode('wcl_get_channel', 'wcl_get_channel');
add_shortcode('wcl_category_dropdown', 'wcl_category_dropdown');
add_shortcode('wcl_get_top_videos', 'wcl_get_top_videos');

function wcl_search_box($atts) {
    if (isset($_GET["search"]))
        $searchStr = esc_attr($_GET["search"]);
    else {
        $searchStr = "";
    }

    $return = '<div id="sb-search" class="sb-search"><form role="form" id="searchform" method="get" action="' . get_page_link(get_option("wcl_search_page")) . '" >
                              <input class="sb-search-input" onfocus="this.placeholder=\'\'" placeholder="Search for FRP TV events & Videos" type="text" value="' . $searchStr . '" name="search" id="search">
                              <input class="sb-search-submit" type="submit" value="">
                              <span class="sb-icon-search"></span>
                          </form></div>';
    return $return;
}

function wcl_search_box_in_menu($atts) {
    if (isset($_GET["search"]))
        $searchStr = esc_attr($_GET["search"]);
    else {
        $searchStr = "";
    }

    $return = '<div class="col-md-5 col-sm-5 col-xs-12">
              <div id="sb-search" class="sb-search">';
    $return .= '<form role="form" class="search2" id="searchform2" method="get" action="' . get_page_link(get_option("wcl_search_page")) . '" >
                              <input class="sb-search-input2" placeholder="Search for FRP TV events & Videos" onfocus="this.placeholder=\'\'" onblur="this.placeholder=\'Search for FRP TV events & Videos\'"  type="text" value="' . $searchStr . '" name="search" id="search">
                              <input class="sb-search-submit2" type="submit" value="">
                              <span class="sb-icon-search2"></span>
                          </form>';
    $return .= ' 
          </div> </div> ';
    return $return;
}

function wcl_category_dropdown($atts) {

    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }

    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }

    if (isset($atts['view']) && $atts['view'] == "Event") {

        $APICallType = "event-categories";
    } else {
        $APICallType = "video-categories";
    }
    if(HTTPS_MODE){ $mode = 'https://';     }
    else{     $mode = 'http://';    }
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
//        $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        $_url = $mode.'worldcastlive.com/tp-widget/api/' . $APICallType;
    } else {
        //$_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/' . $APICallType;
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }

    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
    // echo $_url."<pre>";
    // print_r($response);exit;
    $categoryList = $response['data']['categoryList'];



    $return = '<div class="col-md-6 col-sm-6 col-xs-12">
           
                  <select class="cs-select cs-skin-elastic cs-drowpdown_header" name="category" id="category" onchange="getSearchRecords();">
                      <option value="" selected>All Category</option>';

    foreach ($categoryList as $key => $values) {
        if (isset($_GET['category']) && $_GET['category'] == $key) {
            $isSelected = "selected";
        } else {
            $isSelected = "";
        }
        $return .= '<option value="' . $key . '" data-class="flag-france" ' . $isSelected . '>' . $values . '</option>';
    }

    $return .= ' </select>
             
          </div> ';
    return $return;
}

function wcl_widget($atts) {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=935px';
    }
    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=730px';
    }

    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $return = "";

//obtaint the token first
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/get-token';
    } else {
        $_url = $mode. WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'email' => $user_email,
            'accesskey' => $accesskey
        )
    ));

    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    if ($response['status'] == 1) {
        $token = $response['data']['token'];
        $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
    } else {
        $token = '';
        $wp_event_endpoint_url = "";
    }
    // echo $token;exit;

    if ($wp_event_environment == "live") {
        //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
        /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
          This will be provided by the WCL admin when the domain is registered. */
        if (!empty($wp_event_endpoint_url)) {
            $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        } else {
            $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode ."frptv.worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
    }
    return $return;
}

function wcl_widget_upload_video($atts) {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=935px';
    }
    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=730px';
    }
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $return = "";

//obtaint the token first
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/get-token';
    } else {
        $_url = $mode. WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'email' => $user_email,
            'accesskey' => $accesskey
        )
    ));

    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    if ($response['status'] == 1) {
        $token = $response['data']['token'];
        $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
    } else {
        $token = '';
        $wp_event_endpoint_url = "";
    }
    // echo $token;exit;

    if ($wp_event_environment == "live") {
        //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
        /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
          This will be provided by the WCL admin when the domain is registered. */
        if (!empty($wp_event_endpoint_url)) {
            $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        } else {
            $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='".$mode."frptv.worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return = "<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
    }
    $return .='<script language="JavaScript" type="text/javascript">
function loadPage(){
document.getElementById("myIframe").contentWindow.document.location.href = pageTracker._getLinkerUrl("URL-DE-LA-PAGE-DE-DESTINATION");
}
</script>';
    return $return;
}

function wcl_get_events_detail() {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=86%';
    }

    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=1500px';
    }
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $wp_End_Point=get_option('End_Point');
    if(empty($wp_End_Point)){
        $wp_End_Point='frptv';
    }
    if (isset($_GET['hash'])) {
        $mode = 'https://';
        $return = "";
        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $token = '';
        $wp_event_endpoint_url = "";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }

        $return.='<div id="videcontainerwrapper" class="videcontainerwrapper"  style="display:none;" ><div id="videocontainer" style="display:none;">here is player</div></div>';
        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
            if (!empty($wp_event_endpoint_url)) {

                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/". $token ."/?hash=" . $_GET['hash'] . "&hide=1&verify=$wp_End_Point' frameborder='0' $width $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            } else {
                // $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='http://worldcastlive.com/tp-widget?hash=" . $_GET['hash'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode ."frptv.worldcastlive.com/tp-widget/". $token ."/?hash=" . $_GET['hash'] . "&hide=1&verify=$wp_End_Point' frameborder='0' $width $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            }
        } else {
            $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . WPE_ENV . ".worldcastlive.com/tp-widget/". $token ."/?hash=" . $_GET['hash'] . "&hide=1&verify=$wp_End_Point' frameborder='0' $width $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else if (isset($_GET['video_id'])) {

        $return = "";
        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $token = '';
        $wp_event_endpoint_url = "";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }

        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
            if (!empty($wp_event_endpoint_url)) {
                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1&verify=$wp_End_Point' frameborder='0' $width $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            } else {
                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode ."frptv.worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1&verify=$wp_End_Point' frameborder='0' $width $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            }
        } else {
//            $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1&verify=$wp_End_Point' frameborder='0' width='100%' height='800' scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else if (isset($_GET['channel_id'])) {

        $return = "";
        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url =  $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $token = '';
        $wp_event_endpoint_url = "";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }

        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
            if (!empty($wp_event_endpoint_url)) {
                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='".$mode . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?channel=" . $_GET['channel_id'] . "&hide=1' frameborder='0' width='100%' $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            } else {
                $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='". $mode ."frptv.worldcastlive.com/tp-widget/" . $token . "?channel=" . $_GET['channel_id'] . "&hide=1&verify=$wp_End_Point' frameborder='0' width='100%' $height scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            }
        } else {
//            $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            $return.="<iframe id = 'MyIframe' allow='microphone; camera'  src='".$mode . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?channel=" . $_GET['channel_id'] . "&hide=1&verify=$wp_End_Point' frameborder='0' width='100%' height='1500' scrolling='no' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return = "";
        $return.="<div class='well well-lg'>Invalid access. Click <a href='" . get_home_url() . "'>here for home page</a></div>";
    }
    $return .='<script language="JavaScript" type="text/javascript">
function loadPage(){
document.getElementById("myIframe").contentWindow.document.location.href = pageTracker._getLinkerUrl("URL-DE-LA-PAGE-DE-DESTINATION");
}
</script>';
    if (isset($_GET['video_id'])) {
        $return .='<script>$( "#mega-menu-item-6714" ).addClass( "current-menu-item current_page_item" )</script>';
    }
    else if (isset($_GET['channel_id'])) {
        $return .='<script>$( "#mega-menu-item-7206" ).addClass( "current-menu-item current_page_item" )</script>';
    }
    else if (isset($_GET['hash'])) {
       // $return .='<script>$( "#mega-menu-item-6722" ).addClass( "current-menu-item current_page_item" )</script>';
    }
    return $return;
}

function call_wcl_api($_url) {

    //obtaint the token first
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;


    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey
        )
    ));
    $return = "";
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);
    return $response;
}

//////////////////////////
function wcl_get_video_list($atts) {
    include('include_get_setting.php');
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }

    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
    $eventList = $response['data']['videoList'];
    $elementId = rand(10, 100);
    if (isset($atts['view']) && $atts['view'] == 'list') {
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_video_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
    } else {

        include('include_assets_slider.php');
        include('include_video_slider.php');
    }


    include('include_asset_css.php');
    return $return;
}

//// Event /////
function wcl_get_events_list($atts) {

    include('include_get_setting.php');
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }
    if (isset($atts['limit'])) {
        $wcl_filter.="/limit/" . $atts['limit'];
    }
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url =   $mode.'worldcastlive.com/tp-widget/api/get-events/limit/5/' . $wcl_filter;
    } else {
        $_url =   $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events/limit/5/' . $wcl_filter;
    }

    $return = "";
    $response = call_wcl_api($_url);
    $eventList = $response['data']['eventList'];
    $elementId = rand(10, 100);
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";

    if (isset($atts['view']) && $atts['view'] == 'list') {
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
    } else {
        include('include_assets_slider.php');
        include('include_event_slider.php');
    }

    include('include_asset_css.php');
    return $return;
}

/////Search/////

function wcl_search_old($atts) {

    include('include_get_setting.php');
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }



    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/wp-search';
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/wp-search';
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    $wcl_filter = '';
    if (isset($_GET['category'])) {
        include('include_get_setting.php');
        if ($_GET['category'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/category/" . $_GET['category'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['categoryName'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Category : ' . ucfirst(str_replace('\\', "", urldecode($_GET['categoryName']))) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['videoList'];
        $elementId = rand(10, 100);

        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_video_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else if (isset($_GET['time'])) {
        include('include_get_setting.php');
        if ($_GET['time'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/time/" . $_GET['time'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['time'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Event : ' . ucfirst($_GET['time']) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['eventList'];
        $elementId = rand(10, 100);

        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else {
        $return = "";
        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        if (isset($_GET['search'])) {
            $keyword = $_GET['search'];
        } else {
            $keyword = "";
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'accesskey' => $accesskey,
                'keyword' => $keyword
            )
        ));
        $return = "";
        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        $videoList = $response['data']['videoList'];
        $eventList = $response['data']['eventList'];
        $elementId = rand(10, 100);

        $return.='';
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        //Video List
        $eventList = $videoList;
        include('include_video_grid.php');
        $return.='</div>';



        include('include_noresult_grid.php');
        include('include_asset_css.php');
    }
    return $return;
}

function wcl_search_video_event($atts) {
// print_r($_GET);
// exit;
    include('include_get_setting.php');
    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }
    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }


    if (isset($_GET['category'])) {
        $wcl_filter.="/category/" . $_GET['category'];
    } else {
        $wcl_filter.='';
    }

    if (isset($atts['limit'])) {
        $wcl_filter.="/limit/" . $atts['limit'];
    }

    if (isset($atts['view']) && $atts['view'] == "Event") {
        $ApiName = "get-events";
    } else if (isset($atts['view']) && $atts['view'] == "channel") {
        $ApiName = "get-channels";
//         $ApiName="get-videos";
    } else {
        $ApiName = "get-videos";
    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/' . $ApiName . $wcl_filter;
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/' . $ApiName . $wcl_filter;
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    $wcl_filter = '';


    $return = "";
    //obtaint the token first
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    if (isset($_GET['search'])) {
        $keyword = $_GET['search'];
    } else {
        $keyword = "";
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey,
            'keyword' => $keyword
        )
    ));
    $return = '';
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    if (isset($atts['view']) && $atts['view'] == "Event") {
        $eventList = $response['data']['eventList'];
    } else if (isset($atts['view']) && $atts['view'] == "channel") {

        $eventList = $response['data']['channelList'];

    } else {
        $videoList = $response['data']['videoList'];
    }

    $elementId = rand(10, 100);

    $return.="";
    include('include_assets_grid.php');


    include('include_video_event_page_cantrol_grid.php');


    $return.='<div class="row list box text-shadow">';
    if (isset($atts['view']) && $atts['view'] == "Event") {
        $wp_event_list_grid_count = 3;
        include('include_event_grid_event_page.php');
    } else if (isset($atts['view']) && $atts['view'] == "channel") {
        $return.='<style>'
            . '.jplist-grid-view .list-item { 
    margin: 7px 0 !important;
}
.jplist-grid-view .list-item .img {
    margin-left: 7px !important;
}</style>';
        $wp_event_list_grid_count = 3;

        include('include_channel_grid_channel_page.php');
    } else {

        //Video List
        $eventList = $videoList;
        $wp_event_list_grid_count = 4;

        include('include_video_grid_video_page.php');
    }
    $return.='</div>';

    include('include_noresult_grid.php');
    include('include_asset_css.php');

    return $return;
}

function wcl_search($atts) {
    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }

    if(HTTPS_MODE){

        $mode = 'https://';
    }
    else{

        $mode = 'http://';

    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/wp-search';
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/wp-search';
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    $wcl_filter = '';
    if (isset($_GET['view']) && ($_GET['view'] == 'event') && isset($_GET['category'])) {
        include('include_get_setting.php');
        if ($_GET['category'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/category/" . $_GET['category'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode. 'worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['time'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Event : ' . ucfirst(str_replace('\\', "", urldecode($_GET['categoryName']))) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['eventList'];
        $elementId = rand(10, 100);

        $return.='';
        include('include_assets_grid.php');
        include('include_video_event_page_cantrol_grid.php');


        $return.='<div class="row list box text-shadow">';
        include('include_event_grid_event_page.php');
        $return.='</div>';

        include('include_noresult_grid.php');
        include('include_asset_css.php');
//    }
        return $return;

        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow ">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else if (isset($_GET['category'])) {
        include('include_get_setting.php');
        if ($_GET['category'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/category/" . $_GET['category'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        } else {
            $_url =  $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['categoryName'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Category : ' . ucfirst(str_replace('\\', "", urldecode($_GET['categoryName']))) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['videoList'];
        $elementId = rand(10, 100);


        $return.='';
        include('include_assets_grid.php');
        include('include_video_event_page_cantrol_grid.php');
        $wp_event_list_grid_count = 3;
//        include('include_video_grid.php');
        $return.='<div class="row list box text-shadow ">';
        include('include_video_grid_video_page.php');

//        include('include_video_grid.php');
        $return.='</div>';

        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else if (isset($_GET['top_videos'])) {
        include('include_get_setting.php');
        if ($_GET['top_videos'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/days/" . $_GET['top_videos'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['categoryName'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">' . ucfirst(str_replace('\\', "", urldecode($_GET['categoryName']))) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['videoList'];
        $elementId = rand(10, 100);


        $return.='';
        include('include_assets_grid.php');
        include('include_video_event_page_cantrol_grid.php');
        $wp_event_list_grid_count = 3;
//        include('include_video_grid.php');
        $return.='<div class="row list box text-shadow ">';
        include('include_video_grid_video_page.php');

//        include('include_video_grid.php');
        $return.='</div>';

        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else if (isset($_GET['time'])) {
        include('include_get_setting.php');
        if ($_GET['time'] == 'All') {
            $wcl_filter.='';
        } else {
            $wcl_filter.="/time/" . $_GET['time'];
        }
        if($_GET['search']){
            $wcl_filter.="/search/". $_GET['search'];
        }
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = $mode.'worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        } else {
            $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['time'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Event : ' . ucfirst(str_replace('\\', "", urldecode($_GET['time']))) . '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['eventList'];
        $titlenew = $response['data']['search'];

        //echo "Title: ". $titlenew;
        //exit;
        $elementId = rand(10, 100);

        $return.='';
        include('include_assets_grid.php');
        include('include_video_event_page_cantrol_grid.php');


        $return.='<div class="row list box text-shadow">';
        include('include_event_grid_event_page.php');
        $return.='</div>';

        include('include_noresult_grid.php');
        include('include_asset_css.php');
//    }
        return $return;

        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else {
        $return = "";
        //obtaint the token first
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        if (isset($_GET['search'])) {
            $keyword = $_GET['search'];
        } else {
            $keyword = "";
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'accesskey' => $accesskey,
                'keyword' => $keyword
            )
        ));
        $return = "";
        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);
        if($_GET['searchfor']=="videos"){
            $videoList = $response['data']['videoList'];
        }elseif($_GET['searchfor']=="events"){
            $eventList = $response['data']['eventList'];
        }else{
            $channelList = $response['data']['channelList'];
        }
        $elementId = rand(10, 100);

        $return.='';
        include('include_assets_grid.php');
        include('include_video_event_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid_event_page.php');
        //Video List
        $eventList = $videoList;
        $wp_event_list_grid_count = 3;
//        include('include_video_grid.php');
        include('include_video_grid_video_page.php');
        $return.='</div>';



        include('include_noresult_grid.php');
        include('include_asset_css.php');
    }
    return $return;
}

function wcl_get_video_slider($atts) {
    error_log("wcl_get_video_slider start time");
    error_log($firstTime=time());
    $wp_event_environment = get_option('wp_event_environment');
    $headingTitle = '';
    $viewMoreLink = '';
    if(HTTPS_MODE){ $mode = 'https://';     }
    else{     $mode = 'http://';    }

    if (isset($atts['title'])) {
        $headingTitle = $atts['title'];
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
        $viewMoreLink = "?category=" . $atts['category'] . '&categoryName=' . urlencode($headingTitle);
    } else {
        $viewMoreLink = "?category=All";
    }
    $viewMore = get_page_link(get_option("wcl_search_page")) . $viewMoreLink;
    $wcl_filter.="/limit/9";
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    }

    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";

    $return = "";

    $response = call_wcl_api($_url);
    error_log("wcl_get_video_slider end time");
    error_log(time()-$firstTime);
    $eventList = $response['data']['videoList'];
    // $eventList = array_reverse($eventList);

    if (isset($atts['type']) && $atts['type'] == '1') {
        $return.='<div style="max-height:300px;height:100%" class="slider-container"><div data-visible="2" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            if ($responsearr['video_type'] == 1) {
                $video_type = '<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            } else {
                $video_type = '<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            }
            $return.='<div class="swiper-slide" style="width: 100%; height: 100px;padding-left: 0px;padding-right: 0px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                    <img class="attachment-thumb_375x300" src="' . $responsearr['avatar'] . '" width="375" height="211"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"><p> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </div>
                                    
                                </a>
                                
                   
                                <div class="primary-content">
                                    
				                                                                     <h3 class="h6"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                    <!-- ' . $video_type . ' -->
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div></div>';
        // echo $return;exit;
    } else if (isset($atts['type']) && $atts['type'] == '2') {
        $return.='<div class="cactus-categories-slider" style="padding:0px;">
          <div data-auto-play="" class="cactus-slider cactus-slider-wrap slidesPerView ">
        <div class="cactus-slider-content">
            <div class="fix-slider-wrap"> <a class="cactus-slider-btn-prev" href="javascript:;" style="display: block;"><i class="fa fa-angle-left"></i></a> <a class="cactus-slider-btn-next" href="javascript:;" style="display: block;"><i class="fa fa-angle-right"></i></a>
              <div data-per-view="2" data-settings="[&quot;mode&quot;:&quot;cactus-fix-composer&quot;]" class="cactus-swiper-container" style="">
                <div class="swiper-wrapper">
    		';
        $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            if ($responsearr['video_type'] == 1) {
                $video_type = '<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            } else {
                $video_type = '<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            }

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];


            if (($i % 3) == 0) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item" style="width: 530px;"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>

<h3 class="h6 cactus-slider-post-title"><!--' . $video_type . '-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img class="lazyload" data-original="' . $responsearr['avatar'] . '"  width="279" height="157" />
</a>
</div>

<h3 class="h6 cactus-slider-post-title"><!--' . $video_type . '-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){




            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
        $return.='</div>
</div>
</div>
</div>
</div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '3') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" class="lazyload" data-original="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p>
                                                                    <div class="content-abs-post">
                                                                        
                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            ' . $video_type . '
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 6) {
                $return.='<p style="margin-right: 10px; float: right;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '4') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry" style="margin-bottom: 0px;padding-bottom: 13px;">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a></div>
                                                                        </a><!--<p>
                                                                        </p>-->
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left: 10px;float: left;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '5') {
        $return.='<div class="wpb_column vc_column_container vc_col-sm-6" style="width: 47%;float: left;margin-right: 3%;
">
    <div class="wpb_wrapper">
        <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="4" data-item-in-page="4" data-style="1" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#27aac5" data-title-color="#ffffff" data-shortcode="" data-ids="">
           ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"> <h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 <!-- <img src="' . plugins_url() . '/newstube-shortcodes/shortcodes/img/blue-grey-left-border.png"/>--> </div><br>';

        $return.='          <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="append-slide-auto" data-page="1">
                            <div class="cactus-listing-wrap">
                                <div class="cactus-listing-config style-1">
                                    <div class="cactus-listing-content">
                                        <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                   ' . $video_type . '                                                          
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                ';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="53" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '6') {
        $return.='<div data-settings="" class="cactus-swiper-container">
                <div class="swiper-wrapper">
                
                    <div class="swiper-slide"> <!--Slide item-->
                        
                        <div data-page="1" class="append-slide-auto">
				 			       
                            <!--Listing-->
                            <div class="cactus-listing-wrap">
                                <!--Config-->        
                                <div class="cactus-listing-config style-1"> <!--addClass: style-1 + (style-a -> style-f)-->
                                        
                                    <div class="cactus-listing-content">
                                    
                                        <div class="cactus-sub-wrap">
                                        <div class="cactus-post-item hentry ">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";


            if ($i == 1) {
                $return.='<div class="entry-content">
                                        <div class="primary-post-content"> <!--addClass: related-post, no-picture -->
                                            <!--picture-->
                                            <div class="picture">
                                                <div class="picture-content">
                                                    <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                        <div class="adaptive">

                                                            <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>                                                                        <div class="thumb-overlay"></div>

                                                        <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                    </a>


                                                    
                                                    </div>                                                                                                                                                                                                                   
                                                </div>                                                                
                                            </div>

                                            <div class="content">
                                                <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                    ' . $video_type . '


                                            </div>

                                        </div><!--content-->

                                    </div>  
                                    
<div class="fix-right-style-4">';
            } else {
                $return.='<div class="cactus-post-item hentry ">
                                            <!--content-->
                                            <div class="entry-content">
                                                <div class="primary-post-content"> <!--addClass: related-post, no-picture -->

                                                    <!--picture-->
                                                    <div class="picture">
                                                        <div class="picture-content">
                                                            <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                                <div class="adaptive">

                                                                    <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72">
                                                                        </div>          
                                                                        <div class="thumb-overlay"></div>

                                                                <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                            </a>

                                                                                                                                                                                                                  
                                                        </div>                                                                
                                                    </div>

                                                    <div class="content">
                                                        <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                       ' . $video_type . '
                                                    </div>

                                                </div>

                                            </div><!--content-->

                                        </div>';
            }
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }


        $return.='</div>
            
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '7') {
        /*
         *  added by Pundalik
         */
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item-2-colm hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="380" height="285"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p>

<div class="content-abs-post">
                                                                        
                                                                        <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            ' . $video_type . '
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '8') {

        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307" style="width: 45%;>
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            $video_type = "";


            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];


            if (($i % 3) == 0) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img class="lazyload" data-original="' . $responsearr['avatar'] . '"  width="279" height="157" />
</a>
</div>

<h3 class="h6 cactus-slider-post-title">' . $video_type . '   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>

<h3 class="h6 cactus-slider-post-title">' . $video_type . '   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){




            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }


        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '9') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;float:left;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="height: 201px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                //  $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--    right panel  -->';

        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry"     style="float: right;">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="height: 201px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                // $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '10') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i <= 2) {
                $return.='<div class="cactus-post-item hentry " style="width: 40% !important;">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
</div>
</div>
                                                            <!-- <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>';
                if ($i == 2) {
                    $return.= '<div class="fix-right-style-4" style="width: 20% !important;">';
                }
            } else {
                $return.='   <div class="cactus-post-item hentry layout-col3-in-theme ">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-col3-in-theme-picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                      
</div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
  
                                                                </div> -->
</div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left: 10px;float: left;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
</div>
</div>
</div>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '11') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="max-height: 300px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="390" height="235"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry layout-5-in-theme ">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-5-in-theme-picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img class="lazyload" data-original="' . $responsearr['avatar'] . '" width="94" height="72"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                            </a>
                                                                            <a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>                                                                        
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 5) {
                $return.='<p style="    margin-right: 10px;float: right;padding-top: 0px;margin-bottom: 20px;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else {
        $return.='<div data-visible="3" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $return.='<div class="swiper-slide" style="width: 383px; height: 300px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                    <img class="lazyload attachment-thumb_375x300" data-original="' . $responsearr['avatar'] . '" width="375" height="211"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"></div>
                                </a>
                                <div class="primary-content">
                                    
				
                                                                                      <h3 class="h6"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                    ' . $video_type . '
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div>';
    }
    $wp_event_custom_css = @get_option('wp_event_custom_css');
    $return.='<style>' . $wp_event_custom_css . '</style>';
    $return.='<script>var JQ= jQuery.noConflict(); 
/*JQ(window).load(function() {
	JQ(".gridloader").fadeOut("slow");
})*/
     JQ("img.lazyload").lazyload({
            effect : "fadeIn"
      });</script>';
    error_log("wcl_get_video_slider after HTML time");
    error_log(time());
    return $return;

}

function wcl_get_top_videos($atts) {
    $wp_event_environment = get_option('wp_event_environment');
    $headingTitle = '';
    $viewMoreLink = '';
    if (isset($atts['title'])) {
        $headingTitle = $atts['title'];
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['days'])) {
        $days = $atts['days'];
    } else {
        $days = 7;
    }
    if(HTTPS_MODE){ $mode = 'https://';     }
    else{     $mode = 'http://';    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['days'])) {
        $wcl_filter.="/category/" . $atts['category'];
        $viewMoreLink = "?top_videos=" . $atts['days'] . '&categoryName=' . urlencode($headingTitle);
    } else {
        $viewMoreLink = "?top_videos=All" . '&categoryName=' . urlencode($headingTitle);
    }
    $viewMore = get_page_link(get_option("wcl_search_page")) . $viewMoreLink;
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    if ($wp_event_environment == "live") {
        $_url = $mode.'worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
    } else {
        $_url = $mode . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-top-videos' . $wcl_filter;
    }

    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";

    $return = "";
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey,
            'days' => $days
        )
    ));
    $return = "";
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    $eventList = $response['data']['videoList'];
    if (isset($atts['type']) && $atts['type'] == '11') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="height: 300px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        ' . $video_type . '                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry layout-5-in-theme ">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-5-in-theme-picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                            </a>
                                                                            <a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    ' . $video_type . '
                                                                    
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 5) {
                $return.='<p style="    margin-right: 10px;float: right;padding-top: 0px;margin-bottom: 20px;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type = "";

            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item-2-colm hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p>
<div class="content-abs-post">
                                                                        
                                                                        <h3 class="h4 cactus-post-title entry-title"><a style="font-weight: 400;text-shadow: none;" href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            ' . $video_type . '
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
        $wp_event_custom_css = @get_option('wp_event_custom_css');
        $return.='<style>' . $wp_event_custom_css . '</style>';
    }
    return $return;
}

function wcl_get_events($atts) {
    include('include_event_shortcode.php');
    return $return;
}

function wcl_get_channel($atts) {
    error_log("wcl_get_channel start time");
    error_log($firstTime=time());
    wp_enqueue_script('jquery');
    include('include_channel_shortcode.php');
    return $return;
}
// added facebook og tags
if ( isset($_GET['video_id']) ) {

    add_action('wp_head', 'hook_og_tags',1);
}
function hook_og_tags() {
    if(HTTPS_MODE){ $mode = 'https://';     }
    else{     $mode = 'http://';    }
    $endpoint = "video_detail_by_id";
    $params['video_id'] = $_GET['video_id'];
    $params['verify'] = get_option('End_Point');
    $wcl_api = apply_filters('wcl_ec_api', $endpoint,$params );
    $response = (isset($wcl_api['success']) && $wcl_api['success']) ? json_decode($wcl_api['response']['body']) : array();

    $image = $mode . get_option('End_Point') . ".worldcastlive.com/video/widget/thumbs/".$response->data->image;
    $title =  $response->data->title;

    ?>
    <meta property="og:image" content="<?php echo $image;?>">
    <meta property="og:title" content="<?php echo $title;?>">

    <?php
}

