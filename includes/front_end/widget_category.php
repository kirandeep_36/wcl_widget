<?php


// Creating the widget 
class wpwclcategorylist extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpwclcategorylist', 

// Widget name will appear in UI
__('WCL Videos Category', 'wcl-widget'), 

// Widget description
array( 'description' => __( 'WCL Videos Categories', 'wcl-widget' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
//if ( ! empty( $title ) )
//$title= $args['before_title'] . $title . $args['after_title'];
$wcl_filter='';

$wp_event_environment = get_option('wp_event_environment');
 
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
     $link = "?";
if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/video-categories' ;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/video-categories';
    }
    
      //obtaint the token first  
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;


    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey
        )
    ));
    $return = "";
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);
    $categoryList = $response['data']['categoryList'];
   
	 
    $return='';
       $return.=   
       '<div class="category-widget"><div class="header-div-bottom header-div-bottom-h2">'.$args['before_title'] . $title . $args['after_title'].
      '</div>
          <ul class="wcl-category-list">';
		 foreach ($categoryList as $key=>$responsearr) {
                     $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $key.'&categoryName='.$responsearr;
           
                       $return.='<li class="cat-item cat-item-' . $key.'"><a title="' . $responsearr. '" href="'.$categoryLink.'">' . $responsearr. '</a>
</li>';

           
                }
        $return.='</ul></div>';
$return.='<style>
        .widget_wpwclcategorylist li {
        margin-bottom: 0;
    color: rgba(153,153,153,1.0);
    font-size: 11px;
    text-transform: uppercase;
    padding: 15px 0 16px 0;
    border-bottom: 1px solid rgba(243,243,243,1.0);
}

.widget_wpwclcategorylist li a::before {
    content: "";
    font-family: "FontAwesome";
    left: 0;
    line-height: 1;
    margin-top: -5px;
    position: absolute;
    top: 50%;
}
.widget_wpwclcategorylist li a {
    color: rgba(153, 153, 153, 1);
    display: inline-block;
    font-size: 11px;
    font-weight: bold;
    letter-spacing: 1px;
    padding-left: 14px;
    position: relative;
    text-decoration: none;
    text-transform: uppercase;
    vertical-align: top;
}
.widget_wpwclcategorylist ul {
    line-height: 1.5;
    list-style: outside none none;
}
        </style>';
                 
// This is where you run the code and display the output
$html_content=$return;
   
echo $html_content;
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Category', 'wcl-widget' );
}


// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

return $instance;
}
} // Class wpbdpwidget ends here



function wp_wcl_categorylist() {	
        register_widget( 'wpwclcategorylist' );
}

add_action( 'widgets_init', 'wp_wcl_categorylist' );

