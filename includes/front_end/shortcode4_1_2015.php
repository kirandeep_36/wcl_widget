<?php

/* Code added by Prashant Walke */
add_shortcode('wcl_widget', 'wcl_widget');
add_shortcode('wcl_widget_upload_video', 'wcl_widget_upload_video');

add_shortcode('wcl_get_events_detail', 'wcl_get_events_detail');

add_shortcode('wcl_search', 'wcl_search');
add_shortcode('wcl_search_video_event', 'wcl_search_video_event');

add_shortcode('wcl_search_box', 'wcl_search_box');
add_shortcode('wcl_get_events_list', 'wcl_get_events_list');
add_shortcode('wcl_get_video_list', 'wcl_get_video_list');

add_shortcode('wcl_get_video_slider', 'wcl_get_video_slider');
add_shortcode('wcl_get_events', 'wcl_get_events');

add_shortcode('wcl_category_dropdown', 'wcl_category_dropdown');

function wcl_search_box($atts) {
    if (isset($_GET["search"]))
        $searchStr = esc_attr($_GET["search"]);
    else {
        $searchStr = "";
    }
//    $return = '<form method="get" id="searchform" action="' . get_page_link(get_option("wcl_search_page")) . '">
//        <input type="text" value="' . $searchStr . '" placeholder="Search videos" class="form-control search-bar search-field" name="search" id="search">        
//        </form>';
    
    $return = '<div id="sb-search" class="sb-search"><form role="form" id="searchform" method="get" action="' . get_page_link(get_option("wcl_search_page")) . '" >
                              <input class="sb-search-input" placeholder="Search Videos and Events" type="text" value="' . $searchStr . '" name="search" id="search">
                              <input class="sb-search-submit" type="submit" value="">
                              <span class="sb-icon-search"></span>
                          </form></div>';
    return $return;
}

function wcl_category_dropdown($atts){
   
     include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }

    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
//        $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
       $_url = 'http://worldcastlive.com/tp-widget/api/video-categories';
    } else {
        //$_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
         $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/video-categories';
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }

    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
//    echo "<pre>";
//    print_r($response);exit;
    $categoryList = $response['data']['categoryList'];
    
    
    
    /*$return = '<form method="get" id="searchform" action="' . get_page_link(get_option("wcl_search_page")) . '">
        <input type="text" value="' . $searchStr . '" placeholder="Search videos" class="form-control search-bar search-field" name="search" id="search">        
        </form>';*/
    
    
    $return ='<div class="col-md-5 col-sm-5 col-xs-12">
              <label>
                  <select class="cs-select cs-skin-elastic cs-drowpdown_header" name="category" id="category" onchange="getSearchRecords();">
                      <option value="" disabled selected>All Category</option>';
    foreach($categoryList as $key=>$values){
              $return .=  '<option value="'.$key.'" data-class="flag-france">'.$values.'</option>';
    }
                      
                $return .= ' </select>
              </label>
          </div> ';  
    return $return;
}

function wcl_widget($atts) {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=935px';
    }
    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=730px';
    }

    $return = "";

//obtaint the token first  
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-token';
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'email' => $user_email,
            'accesskey' => $accesskey
        )
    ));

    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    if ($response['status'] == 1) {
        $token = $response['data']['token'];
        $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
    } else {
        $token = '';
        $wp_event_endpoint_url = "";
    }
    // echo $token;exit;

    if ($wp_event_environment == "live") {
        //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
        /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
          This will be provided by the WCL admin when the domain is registered. */
        if (!empty($wp_event_endpoint_url)) {
            $return = "<iframe id = 'MyIframe'  src='http://" . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        } else {
            $return = "<iframe id = 'MyIframe'  src='http://worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return = "<iframe id = 'MyIframe'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=0' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
    }
    return $return;
}

function wcl_widget_upload_video($atts) {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=935px';
    }
    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=730px';
    }

    $return = "";

//obtaint the token first  
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;
    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-token';
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'email' => $user_email,
            'accesskey' => $accesskey
        )
    ));

    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);

    if ($response['status'] == 1) {
        $token = $response['data']['token'];
        $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
    } else {
        $token = '';
        $wp_event_endpoint_url = "";
    }
    // echo $token;exit;

    if ($wp_event_environment == "live") {
        //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
        /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
          This will be provided by the WCL admin when the domain is registered. */
        if (!empty($wp_event_endpoint_url)) {
            $return = "<iframe id = 'MyIframe'  src='http://" . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        } else {
            $return = "<iframe id = 'MyIframe'  src='http://worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return = "<iframe id = 'MyIframe'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?hide=1&wpaction=video_upload' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
    }
    $return .='<script language="JavaScript" type="text/javascript">
function loadPage(){
document.getElementById("myIframe").contentWindow.document.location.href = pageTracker._getLinkerUrl("URL-DE-LA-PAGE-DE-DESTINATION");
}
</script>';
    return $return;
}

function wcl_get_events_detail() {
    if (isset($atts['width'])) {
        $width = $atts['width'];
    }
    if (isset($atts['height'])) {
        $height = $atts['height'];
    }
    if (isset($width) && !empty($width)) {
        $width = 'width=' . $width . 'px';
    } else {
        $width = 'width=86%';
    }
    if (isset($height) && !empty($height)) {
        $height = 'height=' . $height . 'px';
    } else {
        $height = 'height=900px';
    }
    if (isset($_GET['hash'])) {

        $return = "";
        //obtaint the token first  
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = 'http://worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $token = '';
        $wp_event_endpoint_url = "";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }

        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
            if (!empty($wp_event_endpoint_url)) {
                $return.="<iframe id = 'MyIframe'  src='http://" . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget?hash=" . $_GET['hash'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            } else {
                $return.="<iframe id = 'MyIframe'  src='http://worldcastlive.com/tp-widget?hash=" . $_GET['hash'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            }
        } else {
            $return.="<iframe id = 'MyIframe'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget?hash=" . $_GET['hash'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else if (isset($_GET['video_id'])) {

        $return = "";
        //obtaint the token first  
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = 'http://worldcastlive.com/tp-widget/api/get-token';
        } else {
            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-token';
        }

        $token = '';
        $wp_event_endpoint_url = "";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'email' => $user_email,
                'accesskey' => $accesskey
            )
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if ($response['status'] == 1) {
            $token = $response['data']['token'];
            $wp_event_endpoint_url = $response['data']['endpoint']; //added in v.1.1 01-09-2015
        } else {
            $token = '';
            $wp_event_endpoint_url = "";
        }
        
        if ($wp_event_environment == "live") {
            //$wp_event_endpoint_url=get_option('wp_event_endpoint_url');//Commented in V.1.1 01-09-2015
            /* Endpoint URL for prevent catch. leave blank if you don't have Endpoint URL(By default it points to http://www.worldcastlive.com).
              This will be provided by the WCL admin when the domain is registered. */
            if (!empty($wp_event_endpoint_url)) {
                $return.="<iframe id = 'MyIframe'  src='http://" . $wp_event_endpoint_url . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            } else {
                $return.="<iframe id = 'MyIframe'  src='http://worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            }
        } else {
//            $return.="<iframe id = 'MyIframe'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' $width $height scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
            $return.="<iframe id = 'MyIframe'  src='http://" . WPE_ENV . ".worldcastlive.com/tp-widget/" . $token . "?video_id=" . $_GET['video_id'] . "&hide=1' frameborder='0' width='100%' height='400' scrolling='yes' allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>";
        }
    } else {
        $return.="<div class='well well-lg'>Invalid access. Click <a href='" . get_home_url() . "'>here for home page</a></div>";
    }
    $return .='<script language="JavaScript" type="text/javascript">
function loadPage(){
document.getElementById("myIframe").contentWindow.document.location.href = pageTracker._getLinkerUrl("URL-DE-LA-PAGE-DE-DESTINATION");
}
</script>';
    return $return;
}

function call_wcl_api($_url) {

    //obtaint the token first  
    $current_user = wp_get_current_user();
    $accesskey = get_option('wp_event_key');
    //domain specific access key. This will be provided by the WCL admin when the domain is registered
    $user_email = $current_user->user_email;


    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_url,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'accesskey' => $accesskey
        )
    ));
    $return = "";
    $resp = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($resp, TRUE);
    return $response;
}

//////////////////////////
function wcl_get_video_list($atts) {
    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }

    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }

    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
    $eventList = $response['data']['videoList'];
    $elementId = rand(10, 100);
    if (isset($atts['view']) && $atts['view'] == 'list') {
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_video_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
    } else {

        include('include_assets_slider.php');
        include('include_video_slider.php');
    }


    include('include_asset_css.php');
    return $return;
}

//// Event /////
function wcl_get_events_list($atts) {

    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
    }


    $return = "";
    $response = call_wcl_api($_url);
    $eventList = $response['data']['eventList'];
    $elementId = rand(10, 100);
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    
    if (isset($atts['view']) && $atts['view'] == 'list') {
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
    } else {
        include('include_assets_slider.php');
        include('include_event_slider.php');
    }

    include('include_asset_css.php');
    return $return;
}

/////Search/////

function wcl_search($atts) {

    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }



    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/wp-search';
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/wp-search';
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    $wcl_filter = '';
    if (isset($_GET['category'])) {
        include('include_get_setting.php');
        if($_GET['category']=='All')
        {
            $wcl_filter.='';
        }else{
        $wcl_filter.="/category/" . $_GET['category'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        } else {
            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['categoryName'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Category : ' . ucfirst($_GET['categoryName'] ). '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['videoList'];
        $elementId = rand(10, 100);

        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_video_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    }else if (isset($_GET['time'])) {
        include('include_get_setting.php');
        if($_GET['time']=='All')
        {
            $wcl_filter.='';
        }else{
        $wcl_filter.="/time/" . $_GET['time'];
        }

        $wp_event_environment = get_option('wp_event_environment');
        if ($wp_event_environment == "live") {
            $_url = 'http://worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        } else {
            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
        }
        if (isset($_GET['page_id'])) {
            $link = "&";
        } else {
            $link = "?";
        }
        $link = "?";
        $return = '';
        if (isset($_GET['time'])) {
            $return .= '<h3 class="h4 cactus-post-title entry-title">Event : ' . ucfirst($_GET['time'] ). '</h3>';
        }
        $response = call_wcl_api($_url);
        $eventList = $response['data']['eventList'];
        $elementId = rand(10, 100);

       include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        $return.='</div>';
        include('include_noresult_grid.php');
        include('include_asset_css.php');
    } else {
        $return = "";
        //obtaint the token first  
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        if (isset($_GET['search'])) {
            $keyword = $_GET['search'];
        } else {
            $keyword = "";
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'accesskey' => $accesskey,
                'keyword' => $keyword
            )
        ));
        $return = "";
        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        $videoList = $response['data']['videoList'];
        $eventList = $response['data']['eventList'];
        $elementId = rand(10, 100);

        $return.='';
        include('include_assets_grid.php');
        include('include_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
        include('include_event_grid.php');
        //Video List       
        $eventList = $videoList;
        include('include_video_grid.php');
        $return.='</div>';



        include('include_noresult_grid.php');
        include('include_asset_css.php');
    }
    return $return;
}


function wcl_search_video_event($atts) {
// print_r($_GET);
// exit;
    include('include_get_setting.php');

    $heading = "";
    $response = "";
    $wcl_filter = "";
    if (isset($atts['title'])) {
        if (isset($atts['title_link'])) {
            $heading.="<h2><a href='" . $atts['title_link'] . "'>" . $atts['title'] . "</a></h2>";
        } else {
            $heading.="<h2>" . $atts['title'] . "</h2>";
        }
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }

     if (isset($atts['view']) && $atts['view']=="Event") {
         $ApiName="get-events";
     }else{
         $ApiName="get-videos";
     }

    $wp_event_environment = get_option('wp_event_environment');
    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/'.$ApiName;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/'.$ApiName;
    }
    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";
    $wcl_filter = '';
//    if (isset($_GET['category'])) {
//        include('include_get_setting.php');
//        if($_GET['category']=='All')
//        {
//            $wcl_filter.='';
//        }else{
//        $wcl_filter.="/category/" . $_GET['category'];
//        }
//
//        $wp_event_environment = get_option('wp_event_environment');
//        if ($wp_event_environment == "live") {
//            $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
//        } else {
//            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
//        }
//        if (isset($_GET['page_id'])) {
//            $link = "&";
//        } else {
//            $link = "?";
//        }
//        $link = "?";
//        $return = '';
//        if (isset($_GET['categoryName'])) {
//            $return .= '<h3 class="h4 cactus-post-title entry-title">Category : ' . ucfirst($_GET['categoryName'] ). '</h3>';
//        }
//        $response = call_wcl_api($_url);
//        $eventList = $response['data']['videoList'];
//        $elementId = rand(10, 100);
//
//        include('include_assets_grid.php');
//        include('include_page_cantrol_grid.php');
//        $return.='<div class="row list box text-shadow">';
//        include('include_video_grid.php');
//        $return.='</div>';
//        include('include_noresult_grid.php');
//        include('include_asset_css.php');
//    }else if (isset($_GET['time'])) {
//        include('include_get_setting.php');
//        if($_GET['time']=='All')
//        {
//            $wcl_filter.='';
//        }else{
//        $wcl_filter.="/time/" . $_GET['time'];
//        }
//
//        $wp_event_environment = get_option('wp_event_environment');
//        if ($wp_event_environment == "live") {
//            $_url = 'http://worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
//        } else {
//            $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-events' . $wcl_filter;
//        }
//        if (isset($_GET['page_id'])) {
//            $link = "&";
//        } else {
//            $link = "?";
//        }
//        $link = "?";
//        $return = '';
//        if (isset($_GET['time'])) {
//            $return .= '<h3 class="h4 cactus-post-title entry-title">Event : ' . ucfirst($_GET['time'] ). '</h3>';
//        }
//        $response = call_wcl_api($_url);
//        $eventList = $response['data']['eventList'];
//        $elementId = rand(10, 100);
//
//       include('include_assets_grid.php');
//        include('include_page_cantrol_grid.php');
//        $return.='<div class="row list box text-shadow">';
//        include('include_event_grid.php');
//        $return.='</div>';
//        include('include_noresult_grid.php');
//        include('include_asset_css.php');
//    } else {
        $return = "";
        //obtaint the token first  
        $current_user = wp_get_current_user();
        $accesskey = get_option('wp_event_key');
        //domain specific access key. This will be provided by the WCL admin when the domain is registered
        $user_email = $current_user->user_email;
        if (isset($_GET['search'])) {
            $keyword = $_GET['search'];
        } else {
            $keyword = "";
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'accesskey' => $accesskey,
                'keyword' => $keyword
            )
        ));
        $return = "";
        $resp = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($resp, TRUE);

        if (isset($atts['view']) && $atts['view']=="Event") {
         $eventList = $response['data']['eventList'];
        }else{
            $videoList = $response['data']['videoList'];
        }
//      echo  $slug = basename(get_permalink());
//      exit;
        $elementId = rand(10, 100);

        $return.='';
       
        include('include_assets_grid.php');
         include('include_search_box_video_event.php');
        include('include_video_event_page_cantrol_grid.php');
        $return.='<div class="row list box text-shadow">';
         if (isset($atts['view']) && $atts['view']=="Event") {
         include('include_event_grid.php');
         }else{
             
             //Video List   
             $eventList = $videoList;
        $wp_event_list_grid_count = 4;
        include('include_video_grid.php');
        
         }
$return.='</div>';

        include('include_noresult_grid.php');
        include('include_asset_css.php');
//    }
    return $return;
}

function wcl_get_video_slider($atts) {

    $wp_event_environment = get_option('wp_event_environment');
    $headingTitle = '';
    $viewMoreLink = '';
    if (isset($atts['title'])) {
        $headingTitle = $atts['title'];
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
        $viewMoreLink = "?category=" . $atts['category'].'&categoryName='.$headingTitle;
    }else{
         $viewMoreLink = "?category=All";
    }
    $viewMore = get_page_link(get_option("wcl_search_page")) . $viewMoreLink;

    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-videos' . $wcl_filter;
    }

    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
    $eventList = $response['data']['videoList'];
   // $eventList = array_reverse($eventList);

   if (isset($atts['type']) && $atts['type'] == '1') {
        $return.='<div class="container"><div data-visible="2" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
$i=1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            if($responsearr['video_type']==1){
            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            }else {
                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            
            }
            $return.='<div class="swiper-slide" style="width: 100%; height: 100px;padding-left: 0px;padding-right: 0px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                    <img width="375" height="211" alt="' . $responsearr['title'] . '" class="attachment-thumb_375x300" src="' . $responsearr['avatar'] . '"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"></div>
                                </a>
                                <div class="cactus-note-cat"><a title="View all videos in' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                </div>   
                                <div class="primary-content">
                                    
				                                                                     <h3 class="h6"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                    <!-- '.$video_type.' -->
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div></div>';
       // echo $return;exit;
    } else if (isset($atts['type']) && $atts['type'] == '2') {
        $return.='<div class="cactus-categories-slider">
          <div data-auto-play="" class="cactus-slider cactus-slider-wrap slidesPerView ">
        <div class="cactus-slider-content">
            <div class="fix-slider-wrap"> <a class="cactus-slider-btn-prev" href="javascript:;" style="display: block;"><i class="fa fa-angle-left"></i></a> <a class="cactus-slider-btn-next" href="javascript:;" style="display: block;"><i class="fa fa-angle-right"></i></a>
              <div data-per-view="2" data-settings="[&quot;mode&quot;:&quot;cactus-fix-composer&quot;]" class="cactus-swiper-container" style="">
                <div class="swiper-wrapper">
    		';
        $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            if($responsearr['video_type']==1){
            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            }else {
                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            
            }
            
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
           

            if (($i % 3) == 0 ) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item" style="width: 490px;"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){


         

            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
        $return.='</div>
</div>
</div>
</div>
</div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '3') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    <div class="content-abs-post">
                                                                        <div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            '.$video_type.'
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '4') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 3) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '5') {
        $return.='<div class="wpb_column vc_column_container vc_col-sm-6" style="width: 47%;float: left;margin-right: 3%;
">
    <div class="wpb_wrapper">
        <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="4" data-item-in-page="4" data-style="1" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#27aac5" data-title-color="#ffffff" data-shortcode="" data-ids="">
           ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"> <h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 <!-- <img src="'.plugins_url().'/newstube-shortcodes/shortcodes/img/blue-grey-left-border.png"/>--> </div><br>';

        $return.='          <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="append-slide-auto" data-page="1">
                            <div class="cactus-listing-wrap">
                                <div class="cactus-listing-config style-1">
                                    <div class="cactus-listing-content">
                                        <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                   '.$video_type.'                                                          
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                ';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="53" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '6') {
        $return.='<div data-settings="" class="cactus-swiper-container">
                <div class="swiper-wrapper">
                
                    <div class="swiper-slide"> <!--Slide item-->
                        
                        <div data-page="1" class="append-slide-auto">
				 			       
                            <!--Listing-->
                            <div class="cactus-listing-wrap">
                                <!--Config-->        
                                <div class="cactus-listing-config style-1"> <!--addClass: style-1 + (style-a -> style-f)-->
                                        
                                    <div class="cactus-listing-content">
                                    
                                        <div class="cactus-sub-wrap">
                                        <div class="cactus-post-item hentry ">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }

            if ($i == 1) {
                $return.='<div class="entry-content">
                                        <div class="primary-post-content"> <!--addClass: related-post, no-picture -->
                                            <!--picture-->
                                            <div class="picture">
                                                <div class="picture-content">
                                                    <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                        <div class="adaptive">

                                                            <img width="390" height="235" src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '"></div>                                                                        <div class="thumb-overlay"></div>

                                                        <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                    </a>


                                                    <div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                                                    </div>                                                                                                                                                                                                                   
                                                </div>                                                                
                                            </div>

                                            <div class="content">
                                                <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                    '.$video_type.'


                                            </div>

                                        </div><!--content-->

                                    </div>  
                                    
<div class="fix-right-style-4">';
            } else {
                $return.='<div class="cactus-post-item hentry ">
                                            <!--content-->
                                            <div class="entry-content">
                                                <div class="primary-post-content"> <!--addClass: related-post, no-picture -->

                                                    <!--picture-->
                                                    <div class="picture">
                                                        <div class="picture-content">
                                                            <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                                <div class="adaptive">

                                                                    <img width="94" height="72" src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '"></div>                                                                        <div class="thumb-overlay"></div>

                                                                <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                            </a>


                                                            <div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                                                            </div>                                                                                                                                                                                                                   
                                                        </div>                                                                
                                                    </div>

                                                    <div class="content">
                                                        <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                       '.$video_type.'
                                                    </div>

                                                </div>

                                            </div><!--content-->

                                        </div>';
            }
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }


        $return.='</div>
            
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '7') {
        /*
         *  added by Pundalik
         */
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item-2-colm hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p>
<div class="cactus-note-cat" style="display: block;"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>                                                                    
<div class="content-abs-post">
                                                                        
                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            '.$video_type.'
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '8') {
        
         $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307" style="width: 45%;>
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
         $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
           

            if (($i % 3) == 0 ) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title">'.$video_type.'   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title">'.$video_type.'   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){


         

            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
//            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
//            $return.='  <div class="cactus-post-item-2-colm hentry ">
//                                                    <div class="entry-content">
//                                                        <div class="primary-post-content">
//                                                            <div class="picture">
//                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
//                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
//                                                                        <div class="thumb-overlay"></div>
//                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
//                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
//                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
//                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
//                                                                    <div class="content-abs-post">
//                                                                        <div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
//                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
//                                                                            '.$video_type.'
//                                                                    </div>
//                                                                </div>
//                                                            </div>
//                                                        </div>
//                                                    </div>
//                                                </div>';
//            if ($i >= 6) {
//                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
//                break;
//            }
//            $i++;
//        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';

    } else if (isset($atts['type']) && $atts['type'] == '9') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;float:left;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--    right panel  -->';
                
        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry"     style="float: right;">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
        } else if (isset($atts['type']) && $atts['type'] == '10') {
        $return.='<div class="cactus-categories-slider">
          <div data-auto-play="" class="cactus-slider cactus-slider-wrap slidesPerView ">
        <div class="cactus-slider-content">
            <div class="fix-slider-wrap"> <a class="cactus-slider-btn-prev" href="javascript:;" style="display: block;"><i class="fa fa-angle-left"></i></a> <a class="cactus-slider-btn-next" href="javascript:;" style="display: block;"><i class="fa fa-angle-right"></i></a>
              <div data-per-view="2" data-settings="[&quot;mode&quot;:&quot;cactus-fix-composer&quot;]" class="cactus-swiper-container" style="">
                <div class="swiper-wrapper">
    		';
        $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            if($responsearr['video_type']==1){
            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            }else {
                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            
            }
            
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
           

            if (($i % 3) == 0 ) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item" style="width: 490px;"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){


         

            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
        $return.='</div>
</div>
</div>
</div>
</div>
</div>';
        } else if (isset($atts['type']) && $atts['type'] == '11') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry layout-5-in-theme ">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-5-in-theme-picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 5) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    }
    
    else {
        $return.='<div data-visible="3" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
$i=1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $return.='<div class="swiper-slide" style="width: 383px; height: 300px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                    <img width="375" height="211" alt="' . $responsearr['title'] . '" class="attachment-thumb_375x300" src="' . $responsearr['avatar'] . '"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"></div>
                                </a>
                                <div class="primary-content">
                                    
				<div class="cactus-note-cat"><a title="View all videos in' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                </div>                                                                        <h3 class="h6"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                    '.$video_type.'
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div>';
    }
$wp_event_custom_css = @get_option('wp_event_custom_css');
$return.='<style>' . $wp_event_custom_css . '</style>';
    return $return;
}

function wcl_get_events($atts) {
    include('include_event_shortcode.php');
     return $return;
}