<?php 

$wp_event_environment = get_option('wp_event_environment');
    $headingTitle = '';
    $viewMoreLink = '';
    if (isset($atts['title'])) {
        $headingTitle = $atts['title'];
    }
    if (isset($atts['desc'])) {
        $heading.="<h3>" . $atts['desc'] . "</h3>";
    }
    if (isset($atts['time'])) {
        $wcl_filter.="/time/" . $atts['time'];
    }
    if (isset($atts['category'])) {
        $wcl_filter.="/category/" . $atts['category'];
        $viewMoreLink = "?category=" . $atts['category'].'&categoryName='.urlencode($headingTitle);
    }else{
         $viewMoreLink = "?category=All";
    }
    $viewMore = get_page_link(get_option("wcl_search_page")) . $viewMoreLink;

    if ($wp_event_environment == "live") {
        $_url = 'http://worldcastlive.com/tp-widget/api/get-channels' . $wcl_filter;
    } else {
        $_url = 'http://' . WPE_ENV . '.worldcastlive.com/tp-widget/api/get-channels' . $wcl_filter;
    }

    if (isset($_GET['page_id'])) {
        $link = "&";
    } else {
        $link = "?";
    }
    $link = "?";

    $return = "";
    $response = call_wcl_api($_url);
     error_log("wcl_get_channel start time");
    error_log(time()-$firstTime);
    $eventList = $response['data']['channelList'];
   // $eventList = array_reverse($eventList);
//echo "<pre>";print_r($response);echo "</pre>";exit;
   if (isset($atts['type']) && $atts['type'] == '1') {
        $return.='<div class="slider-container"><div data-visible="2" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
$i=1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['channel_name']) > 15) {
                $title = substr($responsearr['channel_name'], 0, 12) . "....";
            } else {
                $title = $responsearr['channel_name'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $return.='<div class="swiper-slide" style="width: 100%; height: 100px;padding-left: 0px;padding-right: 0px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['channel_name'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['channel_id'] . '">
                                    <img width="375" height="211" alt="' . $responsearr['channel_name'] . '" class="attachment-thumb_375x300" src="' . $responsearr['channel_logo'] . '"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"><p> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </div>
                                    
                                </a>
                                <div class="cactus-note-cat"><a title="View all videos in' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                </div>   
                                <div class="primary-content">
                                    
				                                                                     <h3 class="h6"><a title="' . $responsearr['channel_name'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['channel_id'] . '">' . $responsearr['channel_name'] . '</a></h3>
                    <!-- '.$video_type.' -->
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div></div>';
       // echo $return;exit;
    } else if (isset($atts['type']) && $atts['type'] == '2') {
        $return.='<div class="cactus-categories-slider" style="padding:0px;">
          <div data-auto-play="" class="cactus-slider cactus-slider-wrap slidesPerView ">
        <div class="cactus-slider-content">
            <div class="fix-slider-wrap"> <a class="cactus-slider-btn-prev" href="javascript:;" style="display: block;"><i class="fa fa-angle-left"></i></a> <a class="cactus-slider-btn-next" href="javascript:;" style="display: block;"><i class="fa fa-angle-right"></i></a>
              <div data-per-view="2" data-settings="[&quot;mode&quot;:&quot;cactus-fix-composer&quot;]" class="cactus-swiper-container" style="">
                <div class="swiper-wrapper">
    		';
        $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            if($responsearr['video_type']==1){
            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
            }else {
                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
            
            }
            
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
           

            if (($i % 3) == 0 ) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item" style="width: 530px;"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title"><!--'.$video_type.'-->   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){


         

            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
        $return.='</div>
</div>
</div>
</div>
</div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '3') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    <div class="content-abs-post">
                                                                        <div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            '.$video_type.'
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 6) {
                $return.='<p style="margin-right: 10px; float: right;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '4') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry" style="margin-bottom: 0px;padding-bottom: 13px;">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a></div>
                                                                        </a><!--<p>
                                                                        </p>--><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-right: 10px;float: right;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '5') {
        $return.='<div class="wpb_column vc_column_container vc_col-sm-6" style="width: 47%;float: left;margin-right: 3%;
">
    <div class="wpb_wrapper">
        <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="4" data-item-in-page="4" data-style="1" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#27aac5" data-title-color="#ffffff" data-shortcode="" data-ids="">
           ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"> <h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 <!-- <img src="'.plugins_url().'/newstube-shortcodes/shortcodes/img/blue-grey-left-border.png"/>--> </div><br>';

        $return.='          <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="append-slide-auto" data-page="1">
                            <div class="cactus-listing-wrap">
                                <div class="cactus-listing-config style-1">
                                    <div class="cactus-listing-content">
                                        <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                   '.$video_type.'                                                          
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                ';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="53" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '6') {
        $return.='<div data-settings="" class="cactus-swiper-container">
                <div class="swiper-wrapper">
                
                    <div class="swiper-slide"> <!--Slide item-->
                        
                        <div data-page="1" class="append-slide-auto">
				 			       
                            <!--Listing-->
                            <div class="cactus-listing-wrap">
                                <!--Config-->        
                                <div class="cactus-listing-config style-1"> <!--addClass: style-1 + (style-a -> style-f)-->
                                        
                                    <div class="cactus-listing-content">
                                    
                                        <div class="cactus-sub-wrap">
                                        <div class="cactus-post-item hentry ">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }

            if ($i == 1) {
                $return.='<div class="entry-content">
                                        <div class="primary-post-content"> <!--addClass: related-post, no-picture -->
                                            <!--picture-->
                                            <div class="picture">
                                                <div class="picture-content">
                                                    <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                        <div class="adaptive">

                                                            <img width="390" height="235" src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '"></div>                                                                        <div class="thumb-overlay"></div>

                                                        <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                    </a>


                                                    <div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                                                    </div>                                                                                                                                                                                                                   
                                                </div>                                                                
                                            </div>

                                            <div class="content">
                                                <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                    '.$video_type.'


                                            </div>

                                        </div><!--content-->

                                    </div>  
                                    
<div class="fix-right-style-4">';
            } else {
                $return.='<div class="cactus-post-item hentry ">
                                            <!--content-->
                                            <div class="entry-content">
                                                <div class="primary-post-content"> <!--addClass: related-post, no-picture -->

                                                    <!--picture-->
                                                    <div class="picture">
                                                        <div class="picture-content">
                                                            <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                                                <div class="adaptive">

                                                                    <img width="94" height="72" src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '"></div>                                                                        <div class="thumb-overlay"></div>

                                                                <i class="fa fa-play-circle-o cactus-icon-fix"></i>

                                                            </a>


                                                            <div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                                                            </div>                                                                                                                                                                                                                   
                                                        </div>                                                                
                                                    </div>

                                                    <div class="content">
                                                        <h3 class="' . $responsearr['title'] . '"><a title="" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                                                       '.$video_type.'
                                                    </div>

                                                </div>

                                            </div><!--content-->

                                        </div>';
            }
            if ($i >= 6) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }


        $return.='</div>
            
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '7') {
        /*
         *  added by Pundalik
         */
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            $return.='  <div class="cactus-post-item-2-colm hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
                                                                    </p>
<div class="cactus-note-cat" style="display: block;"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>                                                                    
<div class="content-abs-post">
                                                                        
                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
                                                                            '.$video_type.'
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            if ($i >= 4) {
                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
    } else if (isset($atts['type']) && $atts['type'] == '8') {
        
         $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435448307" style="width: 45%;>
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      " data-total-records="6" data-item-in-page="6" data-style="5" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#a9c056" data-title-color="#ffffff" data-shortcode="" data-ids="">';

        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='<div class="cactus-swiper-container" data-settings="">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
         $i = 1;
        $j = 1;
        $isLast = 1;
        foreach ($eventList as $responsearr) {
            if ($i >= 10)
                break;
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }

            if (($i) == 1) {
                $swiperactive = 'swiper-slide-active';
            } else {
                $swiperactive = '';
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
           

            if (($i % 3) == 0 ) {
                $j = 1;

                $rowWidth = '50percent';

                $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                $return.='<div class="width-' . $rowWidth . '">';

                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title">'.$video_type.'   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
    
</div>
';


                $return.='</div></div>';

                $isLast = 1;
            } else {
                $rowWidth = '25percent';
                if ($j == 1) {
                    $return.='<div class="swiper-slide swiper-slide-visible ' . $swiperactive . '">';

                    $return.='<div class="width-' . $rowWidth . '">';
                }
                $return.='<div class="slide-post-item"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
</a>
<div class="adaptive">
 <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
<img src="' . $responsearr['avatar'] . '" alt="' . $responsearr['title'] . '" width="279" height="157" />
</a>
</div>
<div class="cactus-note-cat"><a title="View all videos in ' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a></div>
<h3 class="h6 cactus-slider-post-title">'.$video_type.'   <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
  
</div>
';
                if ($j != 1) {

                    $return.='</div></div>';
                    $isLast = 1;
                } else {
                    $isLast = 0;
                }
                $j++;
            }


            //   if(!(($i%3)==0 || ($i%2)==0)){


         

            $i++;
        }
        if ($isLast == 0) {
            $return.='</div></div>';
        }
//            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
//            $return.='  <div class="cactus-post-item-2-colm hentry ">
//                                                    <div class="entry-content">
//                                                        <div class="primary-post-content">
//                                                            <div class="picture">
//                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
//                                                                        <div class="adaptive"> <img width="380" height="285" alt="chris" src="' . $responsearr['avatar'] . '"></div>
//                                                                        <div class="thumb-overlay"></div>
//                                                                        <p> <i class="fa fa-play-circle-o cactus-icon-fix"></i>
//                                                                        </p><div class="thumb-gradient" style="display:block;"></div>
//                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> </a>
//                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
//                                                                    <div class="content-abs-post">
//                                                                        <div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
//                                                                        <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="' . $responsearr['title'] . '">' . $responsearr['title'] . '</a></h3>
//                                                                            '.$video_type.'
//                                                                    </div>
//                                                                </div>
//                                                            </div>
//                                                        </div>
//                                                    </div>
//                                                </div>';
//            if ($i >= 6) {
//                $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More"> View More</a></p>';
//                break;
//            }
//            $i++;
//        }

        $return.='</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';

    } else if (isset($atts['type']) && $atts['type'] == '9') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;float:left;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry ">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="height: 201px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
              //  $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--    right panel  -->';
                
        $return.=' <div class="cactus-swiper-container" data-settings="" style="width:50%;">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                $return.='<div class="cactus-post-item hentry"     style="float: right;">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content" style="height: 201px;"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                </div>
                                                            </div>
                                                            <div class="content trans-bg">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="fix-right-style-4">';
            } else {
                $return.='   <div class="cactus-post-item hentry">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 4) {
               // $return.='<p style="margin-left:10px"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
        } else if (isset($atts['type']) && $atts['type'] == '10') {
       $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        if (!empty($headingTitle))
            $return.='<div class="header-div-bottom"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';

        $return.='           <div class="cactus-swiper-container" data-settings="">
                <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="append-slide-auto" data-page="1">
                                <div class="cactus-listing-wrap">
                                    <div class="cactus-listing-config style-1">
                                        <div class="cactus-listing-content">
                                            <div class="cactus-sub-wrap">';
        $i = 1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
             $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i <= 2) {
                $return.='<div class="cactus-post-item hentry " style="width: 40% !important;">
                                                    <div class="entry-content">
                                                        <div class="primary-post-content">
                                                            <div class="picture">
                                                                <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                        <div class="adaptive"> <img width="390" height="235" alt="sde" src="' . $responsearr['avatar'] . '"></div>
                                                                        <div class="thumb-overlay"></div>
                                                                    </a><p><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                    </p><div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
</div>
</div>
                                                            <!-- <div class="content">
                                                                <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                        '.$video_type.'                                                     
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>';
                if($i == 2){   $return.=                               '<div class="fix-right-style-4" style="width: 20% !important;">';}
            } else {
                $return.='   <div class="cactus-post-item hentry layout-col3-in-theme ">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-col3-in-theme-picture">
                                                                    <div class="picture-content"> <a href="' . $videoLink . '" title="' . $responsearr['title'] . '">
                                                                            <div class="adaptive"> <img width="94" height="72" alt="tyga" src="' . $responsearr['avatar'] . '"></div>
                                                                            <div class="thumb-overlay"></div>
                                                                        </a><a href="' . $videoLink . '" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a>
                                                                        <div class="cactus-note-cat"><a href="' . $categoryLink . '" title="View all videos in ' . $responsearr['category_name'] . '">' . $responsearr['category_name'] . '</a></div>
</div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['title'] . '</a></h3>
                                                                    '.$video_type.'
  
                                                                </div> -->
</div>
                                                        </div>
                                                    </div>';
                }
            if ($i >= 4) {
                $return.='<p style="margin-right: 10px;float: right;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
            }
            $i++;
        }
        $return.='</div>
</div>
</div>
</div>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
        } else if (isset($atts['type']) && $atts['type'] == '11') {
        $return.='<div class="vc_row wpb_row vc_row-fluid vc_custom_1436435431382">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="wpb_wrapper">
            <div class="cactus-scb  one-page-no-filter      hidden-dislike  " data-total-records="6" data-item-in-page="6" data-style="4" data-ajax-server="" data-json-server="" data-lang-err="Err" data-bg-color="#ff9c31" data-title-color="#ffffff" data-shortcode="" data-ids="">
              ';
        

        $return.='           <div class="cactus-swiper-container" data-settings="">
                    <div class="">
                        <div class="">
                            <div class="" data-page="1">
                                <div class="">
                                    <div class="cactus-listing-config style-1">
                                        <div class="">
                                            <div class="">';
        $i = 1;
        array_slice($eventList, 0, 5, true);
        shuffle($eventList);
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['channel_name']) > 15) {
                $title = substr($responsearr['channel_name'], 0, 12) . "....";
            } else {
                $title = $responsearr['channel_name'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
             $wp_event_endpoint_url='frptv';
            if ($wp_event_environment == "live") {
                $wp_event_endpoint_url='frptv';
            }else{
                $wp_event_endpoint_url=WPE_ENV;
            }
            $videoLink = get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'channel_id=' . $responsearr['channel_id'];
            $categoryLink = get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'];
            if ($i == 1) {
                if ($wp_event_environment == "live") {
                    $return.='<div class="channelview cactus-post-item hentry " style="max-width:915px;margin-left: auto;margin-right: auto;float:none;width:100% !important;margin-bottom: 0px!important;">
                                                   <iframe id="MyIframeChannel" name="MyIframeChannel" src="http://' . $wp_event_endpoint_url . '.worldcastlive.com/tp-widget/?channel=' . $responsearr['channel_id'].'&amp;hide=1&amp;wpaction=view_channel" frameborder="0" width="100%" height="625" scrolling="yes" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen=""></iframe>
                                                </div>
                                                 <div class="fix-right-style-4" style="width:100% !important">';
                }else{
                    $return.='<div class="channelview cactus-post-item hentry " style="max-width:915px;margin-left: auto;margin-right: auto;float:none;width:100% !important;margin-bottom: 0px!important;">
                                                   <iframe id="MyIframeChannel" name="MyIframeChannel" src="http://' . WPE_ENV . '.worldcastlive.com/tp-widget/?channel=' . $responsearr['channel_id'].'&amp;hide=1&amp;wpaction=view_channel" frameborder="0" width="100%" height="625" scrolling="yes" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen=""></iframe>
                                                </div>
                                                 <div class="fix-right-style-4" style="width:100% !important">';
                }
                $return.='<div class="header-div-bottom" style="margin-bottom: 10px;"><h2 class="cactus-scb-title ">' . $headingTitle . '</h2>
                 </div><br>';
                
            } else {
                if (!empty($headingTitle))
            
                $return.='   <div class="cactus-post-item hentry layout-5-in-theme " style="width: 25% !important;">
                                                        <div class="entry-content">
                                                            <div class="primary-post-content">
                                                                <div class="picture layout-5-in-theme-picture">
                                                                    <div class="picture-content"> <a class="playchannel" href="http://' . $wp_event_endpoint_url . '.worldcastlive.com/tp-widget/?channel=' . $responsearr['channel_id'].'"&amp;hide=1&amp;wpaction=view_channel" target="MyIframeChannel" title="' . $responsearr['channel_name'] . '">
                                                                            <div class="adaptive"> <img width="94" height="90" alt="tyga" src="' . $responsearr['channel_logo'] . '"></div>
                                                                            <div class="thumb-overlay" style="height: 83% !important;"><a class="playchannel" href="http://' . $wp_event_endpoint_url . '.worldcastlive.com/tp-widget/?channel=' . $responsearr['channel_id'].'&amp;hide=1&amp;wpaction=view_channel" target="MyIframeChannel" title="' . $responsearr['title'] . '"> <i class="fa fa-play-circle-o cactus-icon-fix"></i> </a></div>
                                                                        </a><div class="cactus-note-cat"><a href="javascirpt:void(0);">' . $responsearr['channel_name']  . '</a></div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="content">
                                                                    <h3 class="h4 cactus-post-title entry-title"><a href="' . $videoLink . '" title="">' . $responsearr['channel_name'] . '</a></h3>
                                                                    '.$video_type.'                                                                    
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>';
            }
            if ($i >= 5) {
              //  $return.='<p style="    margin-right: 10px;float: right;padding-top: 0px;margin-bottom: 20px;"><a href=' . $viewMore . ' title="View More" style="color: black;"> View More</a></p>';
                break;
                }
            $i++;
        }
        $return.='</div>
                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
          $return.='<script> 
              var JJJ= jQuery.noConflict(); 
              JJJ(document).ready(function(){
    JJJ(".playchannel").click(function(e) {
        e.preventDefault();        
        JJJ("#MyIframeChannel").attr("src", JJJ(this).attr("href"));
    })
});</script>';
    }
    
    else {
        $return.='<div data-visible="3" data-autoplay="0" class="cactus-carousel">
            <div class="cactus-swiper-container" style="cursor: grab;">
                <div class="swiper-wrapper">
             ';
$i=1;
        foreach ($eventList as $responsearr) {
            $date = date_create($responsearr['created']);
            if (strlen($responsearr['title']) > 15) {
                $title = substr($responsearr['title'], 0, 12) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $video_type="";
//            if($responsearr['video_type']==1){
//            $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">FREE</a>';
//            }else {
//                $video_type='<a title="View" id="" target="_parent" class="btn btn-primary font-1">PAID</a>';
//            
//            }
            $return.='<div class="swiper-slide" style="width: 383px; height: 300px;">
                            <div class="carousel-item">
                                <a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">
                                    <img width="375" height="211" alt="' . $responsearr['title'] . '" class="attachment-thumb_375x300" src="' . $responsearr['avatar'] . '"> 
                                    <div class="thumb-gradient"></div>
                                    <div class="thumb-overlay"></div>
                                </a>
                                <div class="primary-content">
                                    
				<div class="cactus-note-cat"><a title="View all videos in' . $responsearr['category_name'] . '" href="' . get_page_link(get_option("wcl_search_page")) . $link . 'category=' . $responsearr['category'] . '">' . $responsearr['category_name'] . '</a>
                </div>                                                                        <h3 class="h6"><a title="' . $responsearr['title'] . '" href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '">' . $responsearr['title'] . '</a></h3>
                    '.$video_type.'
                                </div>
                            </div>
                        </div>';
            if ($i >= 10)
                break;
            $i++;
        }
        $return.='</div>
            </div>
            <a href="javascript:;" class="pre-carousel"><i class="fa fa-angle-left"></i></a>
            <a href="javascript:;" class="next-carousel"><i class="fa fa-angle-right"></i></a>
        </div>';
    }
$wp_event_custom_css = @get_option('wp_event_custom_css');
 error_log("wcl_get_channel after HTML time");
    error_log(time());
$return.='<style>' . $wp_event_custom_css . '</style>';
?>