<?php


// Creating the widget 
class wpwclsearch extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpwclsearch', 

// Widget name will appear in UI
__('WCL Search', 'wcl-widget'), 

// Widget description
array( 'description' => __( 'WCL Videos And Event Search', 'wcl-widget' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
//if ( ! empty( $title ) )
//$title= $args['before_title'] . $title . $args['after_title'];

if (isset($_GET["search"]))
        $searchStr = esc_attr($_GET["search"]);
    else {
        $searchStr = "";
    }
      $return.=$args['before_title'] . $title . $args['after_title'].'
        <div class="cactus-widget-posts ">';
    $return .= '<form method="get" id="searchform" action="' . get_page_link(get_option("wcl_search_page")) . '">
        <input type="text" value="' . $searchStr . '" placeholder="Search videos and events" class="form-control search-bar search-field" name="search" id="search">        
        </form>';
  $return.='</div>';
  $return.='<style>.form-control.search-bar.search-field {
    margin-left: 10px;
    margin-right: 10px;
    width: 90% !important;
}</style>';               
// This is where you run the code and display the output
$html_content=$return;
   
echo $html_content;
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Search', 'wcl-widget' );
}


// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

return $instance;
}
} // Class wpbdpwidget ends here



function wp_wcl_search() {	
        register_widget( 'wpwclsearch' );
}

add_action( 'widgets_init', 'wp_wcl_search' );

