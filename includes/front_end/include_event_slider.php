<?php

$return.='<div class="row">
        	<div class="col-lg-12 event-heading">
                ' . $heading . '
            	 </div> <!-- col 12 end -->
			<div class="col-lg-12">
                            <div id="demo-' . $elementId . '">
                                <div id="owl-demo" class="owl-demo owl-carousel">';

foreach ($eventList as $responsearr) {
    $date = date_create($responsearr['timestart']);
    if (strlen($responsearr['title']) > 18) {
        $title = substr($responsearr['title'], 0, 15) . "....";
    } else {
        $title = $responsearr['title'];
    }
    if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
        $button_text = "Stream Now";
    } else {
        $button_text = "View Info";
    }

    $return.='
                                                    <div class="item">
                                                              <div class="event-holder owlslide">';
    if (isset($responsearr['is_live']) && $responsearr['is_live'] == '1') {
        $return.='<div class="live-tag"> </div>';
    }
    $return.='<img width=' . $wp_event_list_width . ' height=' . $wp_event_list_height . ' src="' . $responsearr['api_avatar'] . '">
                                                                    
                                                              </div>

                                                              <div class="mouse-over owslslider">
                                                              <div class="wclslider-title test-title">
								<p class="title text-title">' . $responsearr["title"] . '<br>'
            . '                                           Date: ' . date_format($date, "M d Y") . '  <br>
                                                                  Time: ' . date_format($date, "h:i:s A") . ' <br>
                                                                  Type: ' . $responsearr['category_name'] . '<br>
                                                                      ' . $responsearr['venue'] . '
                                                                    </p>
                                                             </div>
                                                                   <p class="button-text-center">
                                                                       <a href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'hash=' . $responsearr['hash'] . '"> 
                                                                          <button type="button" class="button-title btn btn-danger">' . $button_text . '</button>
                                                                      </a>
                                                                  </p>
                                                              </div>
                                                   </div>';
}
$return.='</div>
            	</div>            
            </div>
        </div> <!-- row end -->';
$return.='<script type="text/javascript">
          
  
                
</script>';
