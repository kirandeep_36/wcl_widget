<?php
      $return.='<div class="row">
        	<div class="col-lg-12 event-heading">
                ' . $heading . '
            	 </div> <!-- col 12 end -->
			<div class="col-lg-12">
                            <div id="demo-' . $elementId . '">
                                <div id="owl-demo" class="owl-demo owl-carousel">';
        foreach ($eventList as $responsearr) {
            if (strlen($responsearr['title']) > 18) {
                $title = substr($responsearr['title'], 0, 15) . "....";
            } else {
                $title = $responsearr['title'];
            }
            $date = date_create($responsearr['created']);
            $return.='
                                                    <div class="item">
                                                              <div class="event-holder owlslide">';
            $return.='<img width=' . $wp_event_list_width . ' height=' . $wp_event_list_height . ' src="' . $responsearr['avatar'] . '">
                                                                    
                                                              </div>

                                                              <div class="mouse-over owslslider">
                                                              <div class="wclslider-title test-title">
								<p class="title text-title">' . $responsearr["title"] . '</p>
                                                             </div>
                                                                   <p class="button-text-center">
                                                                       <a href="' . get_page_link(get_option("wcl_get_events_detail_page")) . $link . 'video_id=' . $responsearr['video_id'] . '"> 
                                                                          <button type="button" class="button-title btn btn-danger">Enter</button>
                                                                      </a>
                                                                  </p>
                                                              </div>
                                                   </div>';
        }
        $return.='</div>
            	</div>            
            </div>
        </div> <!-- row end -->';
        