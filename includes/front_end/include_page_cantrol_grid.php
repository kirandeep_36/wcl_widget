<?php
 $return.='';
 $return .= '
<!-- main content -->
		<div class="box">
			<div class="center">
						
				<div id="demo" class="box jplist" style="margin: 20px 0 50px 0">
				
					<!-- ios button: show/hide panel -->
					<div class="jplist-ios-button">
						<i class="fa fa-sort"></i>
						 Actions
					</div>
					
					<!-- panel -->
					<div class="jplist-panel box panel-top">						
											
																	
						<!-- items per page dropdown -->
						<div 
						   class="jplist-drop-down" 
						   data-control-type="items-per-page-drop-down" 
						   data-control-name="paging" 
						   data-control-action="paging">
						   
						   <ul>
							 <li><span data-number="3"> 3 per page </span></li>
							 <li><span data-number="5"> 5 per page </span></li>
							 <li><span data-number="10" data-default="true"> 10 per page </span></li>
							 <li><span data-number="all"> View All </span></li>
						   </ul>
						</div>';

        if ($wp_event_filter == 1) {

            $return.='<!-- filter by title -->
						<div class="col-md-5 col-sm-5 col-xs-12 text-filter-box" style="margin-bottom:10px;margin-top:10px !important;">
											
							<i class="fa fa-search  jplist-icon"></i>
												
							<!--[if lt IE 10]>
							<div class="jplist-label">Filter by Title:</div>
							<![endif]-->
												
							<input 
								data-path=".title" 
								type="text" 
								value="" 
								placeholder="Filter by Title" 
								data-control-type="textbox" 
								data-control-name="title-filter" 
								data-control-action="filter"
							/>
						</div>';
        }
        if ($wp_event_switch_view == 1) {
            $return.='<!-- views -->
						<div 
						   class="jplist-views" 
						   data-control-type="views" 
						   data-control-name="views" 
						   data-control-action="views"
						   data-default="jplist-thumbs-view">
						   <button type="button" class="jplist-view jplist-thumbs-view" data-type="jplist-thumbs-view"></button>
						
						   <button type="button" class="jplist-view jplist-grid-view" data-type="jplist-grid-view"></button>
						  </div>';
        } else {
            $return.='<!-- views -->
						<div 
						   class="jplist-views" 
						   data-control-type="views" 
						   data-control-name="views" 
						   data-control-action="views"
						   data-default="jplist-thumbs-view">
						</div>';
        }
        
        $return.='<div style="float:left;"><!-- pagination results -->
						<div 
						   class="jplist-label" 
						   data-type="Page {current} of {pages}" 
						   data-control-type="pagination-info" 
						   data-control-name="paging" 
						   data-control-action="paging">
						</div>
												
						<!-- pagination control -->
						<div 
						   class="jplist-pagination" 
						   data-control-type="pagination" 
						   data-control-name="paging" 
						   data-control-action="paging">
						</div>
						</div>
					</div>				 
					
					<!-- data -->   ';