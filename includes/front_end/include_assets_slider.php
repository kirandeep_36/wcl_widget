<?php

wp_enqueue_script('jquery');

wp_enqueue_style('wcljplistcarouselcss', WPE_PLUGIN_URL . "assets/css/owl.carousel.css");
wp_enqueue_style('wcljplistcarouselcss');

wp_enqueue_style('wcljplistthemecss', WPE_PLUGIN_URL . "assets/css/owl.theme.css");
wp_enqueue_style('wcljplistthemecss');


wp_enqueue_script('wclassetviewsjplist', WPE_PLUGIN_URL . "assets/js/owl.carousel.js");
wp_enqueue_script('wclassetviewsjplist');

wp_enqueue_script('wclassetviewsjplistjs', WPE_PLUGIN_URL . "assets/js/jplist.js");
wp_enqueue_script('wclassetviewsjplistjs');
