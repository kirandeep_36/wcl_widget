<?php

wp_enqueue_script('jquery');


wp_enqueue_style('wcljplistjplistcss', WPE_PLUGIN_URL . "assets/css/jplist.core.min.css");
wp_enqueue_style('wcljplistjplistcss');
wp_enqueue_script('wclassetjplist', WPE_PLUGIN_URL . "assets/js/jplist.sort-bundle.min.js");
wp_enqueue_script('wclassetjplist');


wp_enqueue_style('wcljplistbundlecss', WPE_PLUGIN_URL . "assets/css/jplist.textbox-filter.min.css");
wp_enqueue_style('wcljplistbundlecss');
wp_enqueue_script('wclassetbundlejs', WPE_PLUGIN_URL . "assets/js/jplist.sort-bundle.min.js");
wp_enqueue_script('wclassetbundlejs');

wp_enqueue_style('wcljplistcss', WPE_PLUGIN_URL . "assets/css/jplist.pagination-bundle.min.css");
wp_enqueue_style('wcljplistcss');
wp_enqueue_script('wclassetjs', WPE_PLUGIN_URL . "assets/js/jplist.pagination-bundle.min.js");
wp_enqueue_script('wclassetjs');

wp_enqueue_style('wcljplisthistory', WPE_PLUGIN_URL . "assets/css/jplist.history-bundle.min.css");
wp_enqueue_style('wcljplisthistory');
wp_enqueue_script('wclassethistory', WPE_PLUGIN_URL . "assets/js/jplist.history-bundle.min.js");
wp_enqueue_script('wclassethistory');


wp_enqueue_style('wcljplistfilter', WPE_PLUGIN_URL . "assets/css/jplist.filter-toggle-bundle.min.css");
wp_enqueue_style('wcljplistfilter');
wp_enqueue_script('wclassetfilter', WPE_PLUGIN_URL . "assets/js/jplist.filter-toggle-bundle.min.js");
wp_enqueue_script('wclassetfilter');

wp_enqueue_style('wcljplistviews', WPE_PLUGIN_URL . "assets/css/jplist.views-control.min.css");
wp_enqueue_style('wcljplistviews');
wp_enqueue_script('wclassetviews', WPE_PLUGIN_URL . "assets/js/jplist.views-control.min.js");
wp_enqueue_script('wclassetviews');

wp_enqueue_script('wclassetviewsjplist', WPE_PLUGIN_URL . "assets/js/jplist.js");
wp_enqueue_script('wclassetviewsjplist');

wp_enqueue_script('wclassetviewsflip', WPE_PLUGIN_URL . "assets/js/flip.js");
wp_enqueue_script('wclassetviewsflip');


wp_enqueue_script('wclassetviewsjplistlayzy', WPE_PLUGIN_URL . "assets/js/jquery.lazyload.js");
wp_enqueue_script('wclassetviewsjplistlayzy');

