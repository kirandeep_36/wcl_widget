<?php

    $return.='<style>.jplist .list .img img {
    border-radius: 3px;
    height: ' . $wp_event_list_height . 'px !important;
    max-width: 95%;
        width: ' . $wp_event_list_width . 'px;
}
.eventlist.list-item.item.box.col-lg-5ths.col-md-3.col-sm-4.card {
margin: 0px 0px 10px 0px !important;
}
 .eventlist-content-abs-post-overide-on-video{
 width: 97%!important;   
    margin-left: 6px!important;
 }
.imgeventclass {
    border-radius: 3px !important;
    height: ' . $wp_event_page_list_height . 'px !important;
    max-width: 100% !important;
    width: ' . $wp_event_page_list_width . 'px !important; 
}

.imgchannelclass {
    border-radius: 3px !important;
    height: ' . $wp_channel_page_list_height . 'px !important;
    max-width: 100% !important;
    width: ' . $wp_channel_page_list_width . 'px !important; 
}

.cannelclass .content-abs-post-overide-on-video{   
    max-width: 100% !important;
    width: ' . $wp_channel_page_list_width . 'px !important; 
}
.jplist-grid-view .list-item {
    width: ' . (100 / $wp_event_list_grid_count) . '% !important;
}

.jplist-grid-view .list-item .block {
    margin-top: -' . ($wp_event_list_height+8) . 'px;  
}

.owslslider {
    margin-top: -' . $wp_event_list_height . 'px;  
}


.owl-carousel{
  width: ' . $wp_event_list_width . 'px;
}
.owlslide img {
    border-radius: 3px;
    height: ' . $wp_event_list_height . 'px !important;
    max-width: 100%;
      c
}

.owlslide .block {
    margin-right: 6px;
    width: 92%;
}
.owslslider {
    background: #333 none repeat scroll 0 0 !important;    
   
    opacity: 0.99 !important;  
   width: ' . $wp_event_list_width . 'px !important;
        height: ' . $wp_event_list_height . ';
}


.wclslider-title {
    height: ' . ($wp_event_list_height - ($wp_event_list_height / 4) - 10) . 'px !important;
    text-align: center;
}

.owl-item {
    margin-right: 10px;
     width: ' . $wp_event_list_width . 'px !important;
}

.cactus-note-cat { background: #5bb959 none repeat scroll 0 0; background-image:url(/wp-content/themes/newstube/images/tag-bookmark.png); background-repeat: no-repeat; background-position-x: 6px;}


.button-text-center{
    text-align: center;
}

.jplist-grid-view .list-item .block .test-title {
    height: ' . ($wp_event_list_height - ($wp_event_list_height / 4) - 10) . 'px;
         margin: 10px;
    text-align: center;
}

.jplist-thumbs-view .list-item .block .test-title {
    height: ' . ($wp_event_list_height - ($wp_event_list_height / 4) - 10) . 'px;
         margin: 10px;
    text-align: center;
}
.mouse-over{
display: none;
background: #333333 none repeat scroll 0 0;
 width: ' . $wp_event_list_width . 'px;
  height: ' .($wp_event_list_height -1). 'px !important;
}

.jplist-grid-view .list-item .block {
    margin-right: 6px;
    width: 92%;
}
.jplist-thumbs-view .list-item .block {
    background: #333 none repeat scroll 0 0 !important;    
    margin-left: 13px;
    opacity: 1 !important;  
    width: 92%;
}
 @media only screen and (max-width : 1180px) {
 .active-channel-paid-class{
        margin-left: 0px !important;
    }
    .active-channel-free-class{
      margin-left: 0px !important;
    }
    .content-abs-post-overide-on-video { 
margin-left: 6px !important; 
}
.cannelclasscontent{
 margin-left: 0px !important; 
 padding-left: 15px;
 }
 .eventlist-content-abs-post-overide-on-video{
 width: 100%!important;   
    margin-left: 0px!important;
 }
 }
 
 @media only screen and (max-width : 992px) {
jplist-grid-view .list-item .img {
    width: 100%;
    
}
.jplist .list .img {
   
    padding: 0px 0px !important;
 }  
.jplist-grid-view .list-item {
    width: 50% !important;
}
.owl-pagination {
	display: none !important;
}
.content-abs-post-overide-on-video {   
    max-width: 275px !important;   
    margin-left: auto !important;
    margin-right: auto;
}
.jplist .list .block {
    float: none !important;
    margin-left: auto !important;
    margin-right: auto !important;
        max-width: 275px !important;
}
.eventlist-content-abs-post-overide-on-video {
        max-width:375px !important;
         width: 100% !important;
    }
    .active-channel-paid-class{
        margin-left: 0px !important;
    }
    .active-channel-free-class{
      margin-left: 0px !important;
    }
}
@media only screen and (max-width: 800px){
.jplist-grid-view .list-item .img {  
    margin: 0 5px !important;   
}
.mouse-over-css{
margin-right:0px !important;
}
.cannelclass {
    margin-left: auto !important;
    margin-right: auto !important;
    max-width: 380px !important;
    width: 100% !important;
}
.jplist-grid-view .list-item {
    width: 100% !important;
}
.jplist-grid-view .list-item{
margin-left: auto !important;
    margin-right: auto !important;
}
.jplist .list .list-item {
    float: none !important; 
}
.jplist .list .img {    
    margin-left: auto !important;
    margin-right: auto !important;   
    float:none !important;
}
.cannelclass .mouse-over{ 
    margin-top: -360px !important;
}
}

 @media only screen and (max-width : 568px) {
		.jplist-grid-view .list-item {
    width: 100% !important;
    
}
.content-abs-post-overide-on-video{
  max-width: 275px !important;
      margin: auto !important;
  }
.jplist .list .block {
    float: none !important;
    margin-left: auto !important;
    margin-right: auto !important;
        max-width: 275px !important;
            width: 86% !important;
}
.mouse-over{
max-width:275px !imporatant;
}
.owl-pagination {
	display: none !important;
}

.eventlist.list-item.item.box.col-lg-5ths.col-md-3.col-sm-4.card {
    max-width: 375px !important;
      width: 100% !important;
      margin-left: auto !important;
    margin-right: auto !important;

    }
    .eventlist.mouse-over1{
    max-width: 375px !important;
    }
    .eventlist-content-abs-post-overide-on-video {
        max-width:375px !important;
    }
    .active-channel-paid-class{
        margin-left: 1px !important;
    }
    .active-channel-free-class{
      margin-left: 1px !important;
    }
    .jplist .list {   
    margin-left: auto !important;
    margin-right: auto !important;
}
.eventlist.box{
 float: none !important;
}

}
    @media (max-width: 768px) and (min-width: 481px){
    .eventlist.list-item.item.box.col-lg-5ths.col-md-3.col-sm-4.card {
    max-width: 375px !important;
      margin-left: auto !important;
    margin-right: auto !important;
    width: 100% !important;

    }
    .eventlist.mouse-over1{
    max-width: 375px !important;
    }
    .eventlist-content-abs-post-overide-on-video {
        max-width:375px !important;
            width: 100% !important;
    }
    .active-channel-paid-class{
        margin-left: 1px !important;
    }
    .jplist .list {   
    margin-left: auto !important;
    margin-right: auto !important;
}
.eventlist.box{
 float: none !important;
}

    }
 @media only screen and (max-width : 360px) {
		.jplist-grid-view .list-item {
    width: 100% !important;
}

    }
.cannelclasscontent{
 margin-left: 0px !important; 
 padding-left: 15px;
 }

</style>';
    $return.='<style>' . $wp_event_custom_css . '</style>';