=== WCL Event ===
Contributors: WCL
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: WCL event
Requires at least: 4.1+
Tested up to: 4.1
Stable tag: trunk

WCL event
 
== Description ==

WCL event

== Installation ==
1. Download the plugin file, unzip and place it in your wp-content/plugins/ folder. You can alternatively upload it via the WordPress plugin backend.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. WCL Event Plugin menu will appear in Dashboard. Click on it & get started to use.
4. Enter Domain specific access key. This will be provided by the WCL admin when the domain is registered
5. Use this shortcode [wcl_widget] in any page

== Screenshots ==
1. screenshot-2.png
2. screenshot-1.png

== Changelog ==

= 1.0.0 = 
*Plugin Created


== Frequently Asked Questions ==


== Upgrade Notice ==



== Official Site ==
http://worldcastlive.com/