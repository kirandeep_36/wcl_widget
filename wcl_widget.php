<?php

/* Plugin Name:WCL Widget
  Plugin URI:http://worldcastlive.com/
  Description:WCL Widget  plugin helps you to integrate WCL Widget in to WordPress site.
  Author: WCL
  Author URI:http://worldcastlive.com/
  Version: 1.2.0
  Text Domain: wcl-widget
  Domain Path: /lang
 */
/* Developer :Prashant Walke(Perennial System) */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


/* Pusing plugin Update */
require 'plugin-update-checker/plugin-update-checker.php';

//======= Update function =========
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/ikbalsidhu/wcl_widget/',
    __FILE__,
    'wcl_widget.php'
);

//Optional: If you're using a private repository, specify the access token like this:
$MyUpdateChecker->setAuthentication('UJvPyoqqLYTUoM4pmLFA');
// $MyUpdateChecker->setBranch('master');

if (!class_exists('WPEVENT')) :

    final class WPEVENT {

        public $version = '1.1';
        public $WPE_prefix = "wpe";
        protected static $_instance = null;
        public $query = null;

        public static function instance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?'), '1.0');
        }

        public function __construct() {

            // Define constants
            $this->define_constants();
            register_activation_hook(__FILE__, array($this, 'my_plugin_install_function'));
            $this->install();
            $this->load_textdomain();
            // Include required files		
            $this->includes();
            add_action('wp_login', array($this, 'wcl_login_action'), 10, 2);
            add_action('user_register', array($this, 'wcl_register_action'));
        }

        function wcl_login_action($user_login, $user) { 
            $user_email = $user->data->user_email;
            $user_firstname = $user->data->display_name;
            $user_lastname = $user->data->display_name;
            $user_role = 2;
			
            $this->wcl_register_user($user_email, $user_firstname, $user_lastname, $user_role);
			
        }

        function wcl_register_action($user_id) {   
            $user = new WP_User($user_id);
			 
            $user_login = stripslashes($user->user_login);
            $user_email = stripslashes($user->user_email);  
            $user_role = stripslashes($user->user_url);   
			preg_match_all('!\d+!', $user_role, $matches);
		  	  $user_role   =  current(current($matches));   
			
			 
				if ($user_role == 2 || $user_role == 'teacher' || $user_role == 'Teacher') {
					$user_role = 2;
				}
				  if ($user_role == 1 || $user_role == 'student' || $user_role == 'Student') {
					$user_role = 1;
				}
		 
			  if (in_array("Teacher", $user->roles)) {
				   $user_role = 2;
			  }
			  if (in_array("Student", $user->roles)) {
				   $user_role = 1;
			  } 
			   
            $this->wcl_register_user($user_email, $user_login, $user_login, $user_role);
        }

        function wcl_register_user($user_email, $user_firstname, $user_lastname, $user_role) { 
            //obtaint the token first       
 		
            $accesskey = get_option('wp_event_key');
            //domain specific access key. This will be provided by the WCL admin when the domain is registered

            $wp_event_environment = get_option('wp_event_environment');
            if ($wp_event_environment == "live") {
                $_url = 'http://worldcastlive.com/tp-widget/api/register-user';
            } else {
                $_url = 'https://'. WPE_ENV .'.worldcastlive.com/tp-widget/api/register-user';
            }
 
				$curl = curl_init();
				curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $_url,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array(
                    'email' => $user_email,
                    'f_name' => $user_firstname,
                    'l_name' => $user_lastname,
                    'role' => $user_role,
                    'accesskey' => $accesskey
                )
            ));

            $resp = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($resp, TRUE);
			 
			 
            if ($response['status'] == 1) {
                error_log("Register as WCL Widget Member");
                error_log($user_firstname);
            }
        }

        private function define_constants() {
		define('WPE_PLUGIN_FILE', __FILE__);
		define('WPE_PLUGIN_URL', plugin_dir_url(__FILE__));
		define('WPE_VERSION', $this->version);
		define('WPE_PREFIX', $this->WPE_prefix);
		define('WPE_ENV', 'wclst'); //dev or qa
		define('WPL_ENV', 'qa'); //dev or qa
        }

        function includes() {

		include_once( 'includes/admin/class-admin-settings.php' );
		include_once( 'includes/front_end/shortcode.php' );
		include_once( 'includes/front_end/widget.php' );
		include_once( 'includes/front_end/widget_category.php' );
		include_once( 'includes/front_end/widget_search.php' );
		include_once( 'includes/front_end/widget_event.php' );
        }

        function install() {

            add_option('wp_event_environment', 'live');
            add_option('wp_event_list_width', '205');
            add_option('wp_event_list_height', '300');
            add_option('wp_event_list_grid_count', '5');
            add_option('wp_event_switch_view', '1');
            add_option('wp_event_filter', '1');
            add_option('wp_event_custom_css', '');
        }

        function my_plugin_install_function() {
            //post status and options
            $post = array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_author' => 1,
                'post_date' => date('Y-m-d H:i:s'),
                'post_name' => 'View',
                'post_status' => 'publish',
                'post_title' => 'View',
                'post_content' => '[wcl_get_events_detail]',
                'post_type' => 'page',
            );
            //insert page and save the id
            $newvalue = wp_insert_post($post, false);
            //save the id in the database
            update_option('wcl_get_events_detail_page', $newvalue);
            
            $post = array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_author' => 1,
                'post_date' => date('Y-m-d H:i:s'),
                'post_name' => 'Video',
                'post_status' => 'publish',
                'post_title' => 'Video',
                'post_content' => '[wcl_get_video_list  title="Video" desc="View All Videos" view="list"]',
                'post_type' => 'page',
            );
            //insert page and save the id
            $newvalue = wp_insert_post($post, false);
            //save the id in the database
            update_option('wcl_get_video_list_page', $newvalue);
            
            $post = array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_author' => 1,
                'post_date' => date('Y-m-d H:i:s'),
                'post_name' => 'Video',
                'post_status' => 'publish',
                'post_title' => 'Video',
                'post_content' => '[wcl_get_events_list title="Event" desc="View All Events" view="list"]',
                'post_type' => 'page',
            );
            //insert page and save the id
            $newvalue = wp_insert_post($post, false);
            //save the id in the database
            update_option('wcl_get_events_list_page', $newvalue);

            $post = array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_author' => 1,
                'post_date' => date('Y-m-d H:i:s'),
                'post_name' => 'Submit Video',
                'post_status' => 'publish',
                'post_title' => 'Submit Video',
                'post_content' => '[wcl_widget_upload_video]',
                'post_type' => 'page',
            );
            //insert page and save the id
            $newvalue = wp_insert_post($post, false);
            //save the id in the database
            update_option('wcl_widget_upload_video_page', $newvalue);
        }

        function load_textdomain() {
            load_plugin_textdomain('wcl-widget', plugin_dir_path(__FILE__) . '/lang', 'wcl-widget/lang');
        }

    }

    endif;

function WPEVENTBD() {
    return WPEVENT::instance();
}

 add_action('edit_user_profile_update', 'update_extra_profile_fields');
 
 function update_extra_profile_fields($user_id) {
      $author_obj = get_user_by('id', $user_id); 
	  $role = '';
	  if (in_array("Teacher", $author_obj->roles)) {
		   $role = 2;
	  }
	  if (in_array("Student", $author_obj->roles)) {
		   $role = 1;
	  }
	  if($role!=""){
		$email = $author_obj->data->user_email;
		updateRole( $email, $role);
	  }
	  
 }
 
 function updateRole($user_email, $user_role){  
            //domain specific access key. This will be provided by the WCL admin when the domain is registered

            $wp_event_environment = get_option('wp_event_environment');
            if ($wp_event_environment == "live") {
                $_url = 'http://worldcastlive.com/tp-widget/api/user-role';
            } else {
                $_url = 'https://'. WPE_ENV .'.worldcastlive.com/tp-widget/api/user-role';
            }
  $accesskey = get_option('wp_event_key');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $_url,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array(
                    'email' => $user_email, 
                    'role' => $user_role,
                    'accesskey' => $accesskey
                )
            ));

            $resp = curl_exec($curl);
            curl_close($curl);
			$response = json_decode($resp, TRUE); 
            if ($response['status'] == 1) {
                error_log("Update Role of WCL Widget Member");
                error_log($user_email);
            }
 }
$GLOBALS['WPEVENTBDplugin'] = WPEVENTBD();


?>
